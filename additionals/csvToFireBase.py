import csv
import json

sections = [
  {
    'id': 'NB_AB_AR',
    'name': 'Ha Shirion Jct - Arlozorov Jct (North Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,
    'startPoint': {
      'lat': 32.043767,
      'lng': 34.783401
    },
    'endPoint': {
      'lat': 32.078941,
      'lng': 34.797143
    },
    'path': [
      {
        'lat': 32.043767,
        'lng': 34.783401
      },
      {
        'lat': 32.045563,
        'lng': 34.783796
      },
      {
        'lat': 32.04721,
        'lng': 34.784161
      },
      {
        'lat': 32.048953,
        'lng': 34.78465
      },
      {
        'lat': 32.050513,
        'lng': 34.785055
      },
      {
        'lat': 32.051486,
        'lng': 34.78527
      },
      {
        'lat': 32.053296,
        'lng': 34.785426
      },
      {
        'lat': 32.055079,
        'lng': 34.785559
      },
      {
        'lat': 32.05683,
        'lng': 34.785986
      },
      {
        'lat': 32.058321,
        'lng': 34.786478
      },
      {
        'lat': 32.060067,
        'lng': 34.787029
      },
      {
        'lat': 32.061189,
        'lng': 34.787413
      },
      {
        'lat': 32.062063,
        'lng': 34.787916
      },
      {
        'lat': 32.062999,
        'lng': 34.788674
      },
      {
        'lat': 32.063812,
        'lng': 34.78959
      },
      {
        'lat': 32.065187,
        'lng': 34.79121
      },
      {
        'lat': 32.066085,
        'lng': 34.79197
      },
      {
        'lat': 32.066931,
        'lng': 34.792503
      },
      {
        'lat': 32.067868,
        'lng': 34.792893
      },
      {
        'lat': 32.069207,
        'lng': 34.793376
      },
      {
        'lat': 32.070476,
        'lng': 34.793593
      },
      {
        'lat': 32.071861,
        'lng': 34.79367
      },
      {
        'lat': 32.073651,
        'lng': 34.793754
      },
      {
        'lat': 32.074574,
        'lng': 34.793921
      },
      {
        'lat': 32.075508,
        'lng': 34.794251
      },
      {
        'lat': 32.076933,
        'lng': 34.795172
      },
      {
        'lat': 32.078294,
        'lng': 34.79651
      },
      {
        'lat': 32.078941,
        'lng': 34.797143
      }

    ],
    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  },
  {
    'id': 'NB_AR_GL',
    'name': 'Arlozorov Jct - Glilot Jct (North Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,

    'startPoint': {
      'lat': 32.078941,
      'lng': 34.797143
    },
    'endPoint': {
      'lat': 32.139323,
      'lng': 34.810324
    },
    'path': [
      {
        'lat': 32.078941,
        'lng': 34.797143
      },
      {
        'lat': 32.079752,
        'lng': 34.797759
      },
      {
        'lat': 32.08052,
        'lng': 34.798109
      },
      {
        'lat': 32.08148,
        'lng': 34.798405
      },
      {
        'lat': 32.08325,
        'lng': 34.798844
      },
      {
        'lat': 32.085014,
        'lng': 34.799404
      },
      {
        'lat': 32.086711,
        'lng': 34.800097
      },
      {
        'lat': 32.087819,
        'lng': 34.800473
      },
      {
        'lat': 32.089525,
        'lng': 34.801052
      },
      {
        'lat': 32.091306,
        'lng': 34.801434
      },
      {
        'lat': 32.092406,
        'lng': 34.801705
      },
      {
        'lat': 32.094214,
        'lng': 34.80204
      },
      {
        'lat': 32.096009,
        'lng': 34.802393
      },
      {
        'lat': 32.09781,
        'lng': 34.802695
      },
      {
        'lat': 32.099555,
        'lng': 34.803079
      },
      {
        'lat': 32.100723,
        'lng': 34.803377
      },
      {
        'lat': 32.101649,
        'lng': 34.803815
      },
      {
        'lat': 32.10322,
        'lng': 34.804824
      },
      {
        'lat': 32.104778,
        'lng': 34.805881
      },
      {
        'lat': 32.104778,
        'lng': 34.805881
      },
      {
        'lat': 32.106363,
        'lng': 34.806961
      },
      {
        'lat': 32.107902,
        'lng': 34.807955
      },
      {
        'lat': 32.10947,
        'lng': 34.809012
      },
      {
        'lat': 32.111014,
        'lng': 34.810036
      },
      {
        'lat': 32.111947,
        'lng': 34.810588
      },
      {
        'lat': 32.112824,
        'lng': 34.811017
      },
      {
        'lat': 32.113737,
        'lng': 34.811244
      },
      {
        'lat': 32.114694,
        'lng': 34.811262
      },
      {
        'lat': 32.116517,
        'lng': 34.811079
      },
      {
        'lat': 32.117721,
        'lng': 34.811008
      },
      {
        'lat': 32.119557,
        'lng': 34.810882
      },
      {
        'lat': 32.121321,
        'lng': 34.810801
      },
      {
        'lat': 32.123154,
        'lng': 34.810814
      },
      {
        'lat': 32.124959,
        'lng': 34.81073
      },
      {
        'lat': 32.126772,
        'lng': 34.810681
      },
      {
        'lat': 32.128607,
        'lng': 34.810686
      },
      {
        'lat': 32.130483,
        'lng': 34.810573
      },
      {
        'lat': 32.132291,
        'lng': 34.810514
      },
      {
        'lat': 32.13409,
        'lng': 34.810455
      },
      {
        'lat': 32.135912,
        'lng': 34.810439
      },
      {
        'lat': 32.137724,
        'lng': 34.810369
      },
      {
        'lat': 32.139323,
        'lng': 34.810324
      }
    ],

    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  },
  {
    'id': 'SB_RZ_AR',
    'name': 'Rehavam Zeevi Br - Arlozorov Br (South Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,
    'startPoint': {
      'lat': 32.145815,
      'lng': 34.810362
    },
    'endPoint': {
      'lat': 32.079332,
      'lng': 34.796211
    },
    'path': [
      {
        'lat': 32.145815,
        'lng': 34.810362
      },
      {
        'lat': 32.144092,
        'lng': 34.809831
      },
      {
        'lat': 32.142916,
        'lng': 34.809582
      },
      {
        'lat': 32.141755,
        'lng': 34.809477
      },
      {
        'lat': 32.141755,
        'lng': 34.809477
      },
      {
        'lat': 32.13911,
        'lng': 34.809492
      },
      {
        'lat': 32.136854,
        'lng': 34.809532
      },
      {
        'lat': 32.136854,
        'lng': 34.809532
      },
      {
        'lat': 32.133826,
        'lng': 34.809622
      },
      {
        'lat': 32.131133,
        'lng': 34.809637
      },
      {
        'lat': 32.128391,
        'lng': 34.809651
      },
      {
        'lat': 32.125679,
        'lng': 34.809762
      },
      {
        'lat': 32.122918,
        'lng': 34.809829
      },
      {
        'lat': 32.120345,
        'lng': 34.809929
      },
      {
        'lat': 32.11763,
        'lng': 34.809952
      },
      {
        'lat': 32.114926,
        'lng': 34.809983
      },
      {
        'lat': 32.113972,
        'lng': 34.80995
      },
      {
        'lat': 32.112374,
        'lng': 34.809571
      },
      {
        'lat': 32.111224,
        'lng': 34.809133
      },
      {
        'lat': 32.108888,
        'lng': 34.807559
      },
      {
        'lat': 32.106675,
        'lng': 34.806053
      },
      {
        'lat': 32.105064,
        'lng': 34.805001
      },
      {
        'lat': 32.103,
        'lng': 34.803589
      },
      {
        'lat': 32.101975,
        'lng': 34.802976
      },
      {
        'lat': 32.1009,
        'lng': 34.80252
      },
      {
        'lat': 32.099133,
        'lng': 34.802072
      },
      {
        'lat': 32.097126,
        'lng': 34.801613
      },
      {
        'lat': 32.095105,
        'lng': 34.801116
      },
      {
        'lat': 32.092791,
        'lng': 34.800551
      },
      {
        'lat': 32.090891,
        'lng': 34.800193
      },
      {
        'lat': 32.089117,
        'lng': 34.799673
      },
      {
        'lat': 32.086627,
        'lng': 34.798592
      },
      {
        'lat': 32.085052,
        'lng': 34.797945
      },
      {
        'lat': 32.083878,
        'lng': 34.797603
      },
      {
        'lat': 32.081516,
        'lng': 34.797362
      },
      {
        'lat': 32.081516,
        'lng': 34.797362
      },
      {
        'lat': 32.079812,
        'lng': 34.796603
      },
      {
        'lat': 32.079332,
        'lng': 34.796211
      }
    ],
    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  },
  {
    'id': 'SB_AR_HO',
    'name': 'Arlozorov Br - Holon Jct (South Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,

    'startPoint': {
      'lat': 32.079332,
      'lng': 34.796211
    },
    'endPoint': {
      'lat': 32.038531,
      'lng': 34.778088
    },
    'path': [
      {
        'lat': 32.079332,
        'lng': 34.796211
      },
      {
        'lat': 32.077984,
        'lng': 34.794862
      },
      {
        'lat': 32.076501,
        'lng': 34.79366
      },
      {
        'lat': 32.075487,
        'lng': 34.793166
      },
      {
        'lat': 32.074523,
        'lng': 34.792866
      },
      {
        'lat': 32.073455,
        'lng': 34.79271
      },
      {
        'lat': 32.071637,
        'lng': 34.792635
      },
      {
        'lat': 32.069839,
        'lng': 34.792432
      },
      {
        'lat': 32.068586,
        'lng': 34.792077
      },
      {
        'lat': 32.067408,
        'lng': 34.791645
      },
      {
        'lat': 32.066542,
        'lng': 34.791138
      },
      {
        'lat': 32.065753,
        'lng': 34.790486
      },
      {
        'lat': 32.064053,
        'lng': 34.788411
      },
      {
        'lat': 32.063113,
        'lng': 34.787411
      },
      {
        'lat': 32.062343,
        'lng': 34.78691
      },
      {
        'lat': 32.061185,
        'lng': 34.786273
      },
      {
        'lat': 32.058544,
        'lng': 34.785432
      },
      {
        'lat': 32.055712,
        'lng': 34.784694
      },
      {
        'lat': 32.054379,
        'lng': 34.784368
      },
      {
        'lat': 32.05317,
        'lng': 34.784255
      },
      {
        'lat': 32.05137,
        'lng': 34.784297
      },
      {
        'lat': 32.050456,
        'lng': 34.784279
      },
      {
        'lat': 32.049028,
        'lng': 34.784016
      },
      {
        'lat': 32.046059,
        'lng': 34.783237
      },
      {
        'lat': 32.043908,
        'lng': 34.782742
      },
      {
        'lat': 32.04302,
        'lng': 34.782316
      },
      {
        'lat': 32.042007,
        'lng': 34.781721
      },
      {
        'lat': 32.041056,
        'lng': 34.781063
      },
      {
        'lat': 32.040298,
        'lng': 34.780395
      },
      {
        'lat': 32.038531,
        'lng': 34.778088
      },
    ],

    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  }
]

events = []

weather = []

incidents = ''

db_index_helper = {
  'Event' : {
    'latestId' : -1
  },
  'Incident' : {
    'latestId' : -1
  }
}

# Events
with open('events.csv', newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  first = next(reader)
  cnt = 0
  for row in reader:
    eventLine = {
      'title': row[1],
      'icon': row[2],
      'description': row[3],
      'startTime': row[4],
      'endTime': row[5],
      'lat': float(row[6]),
      'lng': float(row[7])
    }
    sectionsAffected = row[0].split('|');
    print(sectionsAffected)
    for s in sectionsAffected:
      print(s)
      sections[int(s)]['events'].append(eventLine)
    events.append(eventLine)

# Weather
with open('weather.csv', newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  first = next(reader)
  for row in reader:
    weatherline = {
      'hour': row[1],
      'temp': row[2],
      'conditions': row[3],
      'conditionsIcon': row[4],
      'precipitation': row[5],
      'wind': row[6],
      'windDirection': row[7]
    }
    sections[int(row[0])]['weather'].append(weatherline)
    # store only section0 weather as global weather
    if (int(row[0]) == 0):
      weather.append(weatherline)

# Prediction
with open('prediction.csv', newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  # get date (first row)
  date = next(reader)
  for row in reader:
    print(', '.join(row))
    prediction = {
      'hour': row[1],
      'result': row[2]
    }
    sections[int(row[0])]['prediction'].append(prediction)

  jsonResult = {
    'date': date[1],
    'sections': sections,
    'events': events,
    'weather': weather,
    'db_index_helper': db_index_helper,
    'incidents': incidents
  }

print('JSON ', json.dumps(jsonResult))

with open(date[1] + '_result.json', 'w') as outfile:
  json.dump(jsonResult, outfile)
