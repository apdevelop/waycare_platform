import csv
import json

sections = [
  {
    'id': 'NB_AB_AR',
    'name': 'Ha Shirion Jct - Arlozorov Jct (North Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,
    'startPoint': {
      'lat': 32.043772,
      'lng': 34.783445
    },
    'endPoint': {
      'lat': 32.078886,
      'lng': 34.79724
    },
    'path': [
      {
        'lat': 32.045586,
        'lng': 34.783612
      },
      {
        'lat': 32.047242,
        'lng': 34.783988
      },
      {
        'lat': 32.048988,
        'lng': 34.784479
      },
      {
        'lat': 32.050539,
        'lng': 34.78489
      },
      {
        'lat': 32.051491,
        'lng': 34.7851
      },
      {
        'lat': 32.053306,
        'lng': 34.785221
      },
      {
        'lat': 32.055093,
        'lng': 34.785372
      },
      {
        'lat': 32.056872,
        'lng': 34.785774
      },
      {
        'lat': 32.058359,
        'lng': 34.786277
      },
      {
        'lat': 32.060107,
        'lng': 34.786819
      },
      {
        'lat': 32.061262,
        'lng': 34.787216
      },
      {
        'lat': 32.062167,
        'lng': 34.787742
      },
      {
        'lat': 32.063085,
        'lng': 34.788563
      },
      {
        'lat': 32.063903,
        'lng': 34.789486
      },
      {
        'lat': 32.065259,
        'lng': 34.791083
      },
      {
        'lat': 32.066147,
        'lng': 34.791811
      },
      {
        'lat': 32.066987,
        'lng': 34.792354
      },
      {
        'lat': 32.0679,
        'lng': 34.792753
      },
      {
        'lat': 32.069225,
        'lng': 34.793219
      },
      {
        'lat': 32.070491,
        'lng': 34.793402
      },
      {
        'lat': 32.071862,
        'lng': 34.793502
      },
      {
        'lat': 32.073669,
        'lng': 34.793609
      },
      {
        'lat': 32.07461,
        'lng': 34.793765
      },
      {
        'lat': 32.075556,
        'lng': 34.794118
      },
      {
        'lat': 32.076998,
        'lng': 34.795033
      },
      {
        'lat': 32.078375,
        'lng': 34.796376
      }],

    'prediction': [],
    'incidents': [
      {'incidentType': 'Crash',
       'incidentIcon': 'directions_car',
       'incidentTime': '12:35'},
      {'incidentType': 'road event',
       'incidentIcon': 'stars',
       'incidentTime': '14:35'},
      {'incidentType': 'Crash',
       'incidentIcon': 'directions_car',
       'incidentTime': '14:35'}
    ],
    'events': [],
    'weather': []
  },
  {

    'id': 'NB_AR_GL',
    'name': 'Arlozorov Jct - Glilot Jct (North Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,
    'startPoint': {
      'lat': 32.078886,
      'lng': 34.79724
    },
    'endPoint': {
      'lat': 32.139298,
      'lng': 34.810427
    },
    'path': [
      {'lat': 32.082488, 'lng': 34.799006},
      {'lat': 32.087074, 'lng': 34.800736},
      {'lat': 32.092349, 'lng': 34.802328},
      {'lat': 32.099504, 'lng': 34.803466},
      {'lat': 32.102498, 'lng': 34.80472},
      {'lat': 32.111196, 'lng': 34.810532},
      {'lat': 32.11365, 'lng': 34.811666},
      {'lat': 32.120628, 'lng': 34.811174},
      {'lat': 32.128096, 'lng': 34.811098},
      {'lat': 32.136311, 'lng': 34.810918}
    ],
    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  },
  {
    'id': 'SB_RZ_AR',
    'name': 'Rehavam Zeevi Br - Arlozorov Br (South Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,
    'startPoint': {
      'lat': 32.145832,
      'lng': 34.810273
    },
    'endPoint': {
      'lat': 32.079388,
      'lng': 34.796113
    },
    'path': [
      {
        'lat': 32.144151,
        'lng': 34.80944
      },
      {
        'lat': 32.142007,
        'lng': 34.809128
      },
      {
        'lat': 32.137919,
        'lng': 34.809182
      },
      {
        'lat': 32.133757,
        'lng': 34.80929
      },
      {
        'lat': 32.128952,
        'lng': 34.809193
      },
      {
        'lat': 32.12599,
        'lng': 34.8093
      },
      {
        'lat': 32.121946,
        'lng': 34.809333
      },
      {
        'lat': 32.117357,
        'lng': 34.809494
      },
      {
        'lat': 32.11445,
        'lng': 34.80944
      },
      {
        'lat': 32.111269,
        'lng': 34.808592
      },
      {
        'lat': 32.10529,
        'lng': 34.804493
      },
      {
        'lat': 32.102208,
        'lng': 34.802638
      },
      {
        'lat': 32.100845,
        'lng': 34.802262
      },
      {
        'lat': 32.094247,
        'lng': 34.800589
      },
      {
        'lat': 32.090502,
        'lng': 34.799784
      },
      {
        'lat': 32.085557,
        'lng': 34.797874
      },
      {
        'lat': 32.083885,
        'lng': 34.797305
      },
      {
        'lat': 32.080666,
        'lng': 34.79693
      }
    ],
    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  },
  {
    'id': 'SB_AR_HO',
    'name': 'Arlozorov Br - Holon Jct (South Bound)',
    'strokeColor': 'red',
    'strokeWeight': 6,
    'strokeOpacity': 1,
    'startPoint': {
      'lat': 32.079388,
      'lng': 34.796113
    },
    'endPoint': {
      'lat': 32.038554,
      'lng': 34.778027
    },
    'path': [
      {'lat': 32.078224, 'lng': 34.794825},
      {'lat': 32.077124, 'lng': 34.79372},
      {'lat': 32.074424, 'lng': 34.792605},
      {'lat': 32.070733, 'lng': 34.792164},
      {'lat': 32.068379, 'lng': 34.791639},
      {'lat': 32.066133, 'lng': 34.790416},
      {'lat': 32.063642, 'lng': 34.787476},
      {'lat': 32.061368, 'lng': 34.786006},
      {'lat': 32.055422, 'lng': 34.784269},
      {'lat': 32.050684, 'lng': 34.783946},
      {'lat': 32.048684, 'lng': 34.783646},
      {'lat': 32.044192, 'lng': 34.782594},
      {'lat': 32.041682, 'lng': 34.781189},
      {'lat': 32.039299, 'lng': 34.778979}
    ],
    'prediction': [],
    'incidents': '',
    'events': [],
    'weather': []
  }
]

events = []

weather = []

#Events
with open('events.csv', newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  first = next(reader)
  for row in reader:
    eventLine = {
      'title': row[1],
      'icon': row[2],
      'description': row[3],
      'startTime': row[4],
      'endTime': row[5],
      'lat': float(row[6]),
      'lng': float(row[7])
    }
    sectionsAffected = row[0].split('|');
    print(sectionsAffected)
    for s in sectionsAffected:
      print(s)
      sections[int(s)]['events'].append(eventLine)
    events.append(eventLine)





#Weather
with open('weather.csv', newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  first = next(reader)
  for row in reader:
    weatherline = {
      'hour': row[1],
      'temp': row[2],
      'conditions': row[3],
      'conditionsIcon': row[4],
      'precipitation': row[5],
      'wind': row[6],
      'windDirection': row[7]
    }
    sections[int(row[0])]['weather'].append(weatherline)
    #store only section0 weather as global weather
    if (int(row[0]) == 0):
      weather.append(weatherline)

#Prediction
with open('prediction.csv', newline='') as csvfile:
  reader = csv.reader(csvfile, delimiter=',')
  # get date (first row)
  date = next(reader)
  for row in reader:
    print(', '.join(row))
    prediction = {
      'hour': row[1],
      'result': row[2]
    }
    sections[int(row[0])]['prediction'].append(prediction)

  jsonResult = {
    'date': date[1],
    'sections': sections,
    'events': events,
    'weather': weather
  }

print('JSON ', json.dumps(jsonResult))

with open(date[1] + '_result.json', 'w') as outfile:
  json.dump(jsonResult, outfile)
