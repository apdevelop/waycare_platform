import {Injectable} from '@angular/core';
import {AngularFire} from "angularfire2/index";
import { CfgService } from './cfg.service';

@Injectable()
export class MapService {



  events:any[] = [];
  weather:any[] = [];
  incidents:any[] = [];

  constructor(private af:AngularFire, private cfgs: CfgService) {

    this.initDbItems();

  }

  initDbItems() {


    let curHour = new Date().getHours();
    let hour = curHour - curHour % 2;

    // Events:
    this.af.database.list(this.cfgs.getDbPath() + '/events', {preserveSnapshot: true})
      .subscribe(snapshots=> {
        while(this.events.length > 0) { this.events.pop(); } // must clear w/o change reference
        snapshots.forEach(snapshot => {
          let val = snapshot.val();
          val['dbkey'] = snapshot.key; // needed for Edit
          this.events.push(val)
          //console.log(this.events)
        });
      })

    // Weather:
    this.af.database.list(this.cfgs.getDbPath() + '/weather', {preserveSnapshot: true})
      .subscribe(snapshots=> {
        while(this.weather.length > 0) { this.weather.pop(); } 
        snapshots.forEach(snapshot => {
          this.weather.push(snapshot.val())
        })
      })

    // Incidents:
    this.af.database.list(this.cfgs.getDbPath() + '/incidents', {preserveSnapshot: true})
      .subscribe(snapshots=> {
        while(this.incidents.length > 0) { this.incidents.pop(); } 
        this.incidents.forEach
        snapshots.forEach(snapshot => {
          let val = snapshot.val();
          val['dbkey'] = snapshot.key; // needed for Edit
          this.incidents.push(val);
        })
        //console.log("incidents was updated", this.incidents);
      })

  }

  getEventStyle(event) {
    return {
      'color': 'red'
    }
  }


  getEvents() {
    return this.events;
  }

  getWeather() {
    return this.weather;
  }

  getIncidents(){
    return this.incidents;
  }
  
  

}


