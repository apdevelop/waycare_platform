import {Component, OnInit, Input, AfterViewInit, OnChanges, SimpleChanges} from '@angular/core';

@Component({
  selector: 'app-section-chart',
  templateUrl: './section-chart.component.html',
  styleUrls: ['./section-chart.component.css']
})
export class SectionChartComponent implements OnInit, OnChanges{
  ngOnChanges(changes:SimpleChanges):void {
    if ( (this.section != null) && (this.section.prediction[0] != null) )
      this.update();
  }
  ngOnInit():void {
  }
  @Input() section;
  // lineChart
  public lineChartData:Array<any> = [
    {data: [0, 0, 0, 0, 0, 0], label: 'Risk Level'}
  ];
  public lineChartLabels:Array<any> = ['0:00','2:00','4:00','6:00','8:00', '10:00', '12:00', '14:00', '16:00', '18:00','20:00','22:00'];
  public lineChartOptions:any = {
    animation: false,
    responsive: true
  };
  public lineChartColors:Array<any> = [
    { // grey
      backgroundColor: 'rgba(148,159,177,1)',
      borderColor: 'rgba(148,159,177,1)',
      pointBackgroundColor: 'rgba(148,159,177,1)',
      pointBorderColor: '#fff',
      pointHoverBackgroundColor: '#fff',
      pointHoverBorderColor: 'rgba(148,159,177,0.8)'
    }
  ];
  public lineChartLegend:boolean = true;
  public lineChartType:string = 'line';

  public update():void {
    console.log('in update')
    let _lineChartData:Array<any> = new Array(this.lineChartData.length);
    for (let i = 0; i < 1; i++) {
      _lineChartData[i] = {data: new Array(this.lineChartData[i].data.length), label: this.lineChartData[i].label};
      for (let j = 0; j < 12; j++) {
        //console.log(this.section)
        _lineChartData[i].data[j] = this.section.prediction[j].result;
      }
    }
    this.lineChartData = _lineChartData;
  }

  // events
  public chartClicked(e:any):void {
    console.log(e);
  }

  public chartHovered(e:any):void {
    console.log(e);
  }

}
