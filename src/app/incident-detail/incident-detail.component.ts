import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-incident-detail',
  templateUrl: './incident-detail.component.html',
  styleUrls: ['./incident-detail.component.css']
})
export class IncidentDetailComponent implements OnInit {

  @Input() incidents;
  
  constructor() { }

  ngOnInit() {
  }

}
