import { Component, OnInit } from '@angular/core';
import {Auth} from "./auth.service";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {

  constructor(private auth: Auth) { }

  ngOnInit() {
  }

}
