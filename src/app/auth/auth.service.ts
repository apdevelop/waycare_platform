// app/auth.service.ts

import { Injectable }      from '@angular/core';
import { tokenNotExpired } from 'angular2-jwt';
import { Router } from '@angular/router';
import { siteConfig } from "../config";
import { CfgService } from '../cfg.service';
import { AngularFire } from "angularfire2/index";
import { AuthMethods, AuthProviders } from 'angularfire2/index';

// Avoid name not found warnings
declare var Auth0Lock: any;
declare var Auth0: any;

@Injectable()
export class Auth {
  
  authConfig;
  lock;
  auth0;
  userProfile: Object;

  authOptions:{} = {
    languageDictionary: {
      title: "WayCare"
    },
    theme: {
      logo: '../images/LogoFace.png',
      primaryColor: '#124259' // match the primary color of WayCare logo
    }  
  };


  constructor(private router: Router, private cfgs: CfgService, private af:AngularFire) {
  
    // Set userProfile attribute of already saved profile
    this.userProfile = JSON.parse(localStorage.getItem('profile'));

    this.authConfig =  siteConfig[this.cfgs.getSite()].authConfig;
    this.lock = new Auth0Lock(this.authConfig.clientID, this.authConfig.domain, this.authOptions);

    this.auth0 = new Auth0({ domain : this.authConfig.domain, clientID: this.authConfig.clientID });

    // Add callback for lock `authenticated` event
    this.lock.on("authenticated", (authResult) => {

      this.lock.getProfile(authResult.idToken, (error, profile) => {

      if (error) {
          console.log("lock.getProfile error: ", error);
          return;
        }

        localStorage.setItem('profile', JSON.stringify(profile));
        this.userProfile = profile;

        // Set the options to retreive a firebase delegation token
        var options = {
          id_token : authResult.idToken,
          api : 'firebase',
          scope : 'openid name email displayName',
          target: this.authConfig.clientID
        };

        // Make a call to the Auth0 '/delegate'
        this.auth0.getDelegationToken(options, (err, result) => {

          if(!err) {
            // Exchange the delegate token for a Firebase auth token
            this.af.auth.login(result.id_token, 
                {
                  provider: AuthProviders.Custom,
                  method: AuthMethods.CustomToken
                })
            .then((success) => {
              //console.log("Firebase success: " + JSON.stringify(success));
              console.log("Firebase with auth0 success");
              localStorage.setItem('id_token', authResult.idToken); // Auth done
            })
            .catch((error) => {
              console.log("Firebase failure: " + JSON.stringify(error));
            });


          }
          else {
            console.log("auth0.getDelegationToken error: ", err);
          }
        });

      });

    });
  }



  login() {
    // Display the default lock widget
    this.lock.show();
  }


  public authenticated() {
    // Check if there's an unexpired JWT
    // This searches for an item in localStorage with key == 'id_token'
    return tokenNotExpired();
  }


  public logout() {
    // Remove token from localStorage
    localStorage.removeItem('id_token');
    localStorage.removeItem('profile');
    
    this.router.navigate(['/']);
  }
}
