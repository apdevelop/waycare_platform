
import { Component, OnInit, Input } from '@angular/core';
import { FormGroup }                 from '@angular/forms';
import { QuestionBase }              from '../dynamic-form/question-base';
import { QuestionControlService }    from '../dynamic-form/question-control.service';
import { QuestionService } from '../dynamic-form/question.service';
 

import { EventService } from "../event.service";
import { Observable } from 'rxjs/Rx';
import { MdlDialogService } from "angular2-mdl";
import { Router } from '@angular/router';
import { CfgService } from '../cfg.service';

import {siteConfig} from "../config";


@Component({
  selector: 'app-new-add',
  templateUrl: './new-add.component.html',
  styleUrls: ['./new-add.component.css']
})
export class NewAddComponent implements OnInit {

  mapConfig;
  center;
  zoom;
  @Input() lat: number;
  @Input() lng: number;
  

  isAddMode: boolean = false;

  questions: QuestionBase<any>[] = [];

  form: FormGroup;

  @Input() type: string; 
  @Input() subType: string; 

 
  readyForEdit: boolean;

  copyMode: boolean;

  eventKey; // the key we need to keep using

  oldAffectedSections = [];
  oldEvent;



  constructor(private cfgs: CfgService, private qs: QuestionService, 
    private qcs: QuestionControlService,
    private els: EventService, private dialogService: MdlDialogService,
    private router:Router) { 

   }
  



  // get the active questions while updating fields if needed
  getQuestions(cfg) {

      
      let qCfg =  JSON.parse(JSON.stringify(cfg)); // clone


      // indicate that this is a copy in the title
      if (this.copyMode) {
        if (qCfg['Common']['titleQ'])
          qCfg['Common']['titleQ']['cfg']['value'] += "(copy)";
      }

      //console.log("getQuestions. type:", this.type);

      return this.qs.getQuestionsForCfg(this.subType, qCfg); 


  }

  
  ngOnInit() {


    this.mapConfig = siteConfig[this.cfgs.getSite()].mapConfig;
    this.center = this.mapConfig.center;
    this.zoom = this.mapConfig.zoom;

  	// are we in 'edit/copy mode or 'add' 
  	this.isAddMode = (this.els.getEventUnderEdit() == null);


  	this.readyForEdit = false;


    if (this.isAddMode) {

      //console.log("Type:", this.type);

      this.questions = this.getQuestions(this.qs.getCfg()); 

    }

    else { //--- edit/copy mode


      //--- check that all required data exists

	    let event = this.els.getEventUnderEdit();
	    this.els.clearEventUnderEdit();

	    if (! event ) {
	    	//alert("no event under edit found");
	    	return;
	    }

	    if (! event['eventEditCfg']) {
	    	this.dialogService.alert("Missing eventEditCfg property. can't edit this event.");
	    	return;
	    }

	    this.eventKey = event['dbkey'];

	    if (! this.eventKey ) {
	    	this.dialogService.alert("Missing eventKey property. can't edit this event");
	    	return;
	    }

      this.type = event['type'];

      if (! this.type ) {
        this.dialogService.alert("Missing type property. can't edit this event");
        return;
      }



      this.copyMode = event['__isCopy'];
      delete event['__isCopy'];


	    this.oldAffectedSections = event['affectedSections'];
      this.oldEvent = JSON.parse(JSON.stringify(event)); //clone. required for update

	  	this.readyForEdit = true;

      this.questions = this.getQuestions(event['eventEditCfg']); 


  	} 

  	//console.log("this.questions:", this.questions);

    this.form = this.qcs.toFormGroup(this.questions);  

    this.setFormCoords(this.lat, this.lng);

  }




  onSubmit() {

    //console.log("Add, form:", this.form); 


    let eventData = this.qs.getFormAnswers(this.type, this.form.value);


    //--- replace latLongtitde with two entries
    let latLongAry = eventData['latLongitude'].split(",");
    
    // check it first
    
    if  ( (latLongAry.length !=2) || isNaN(latLongAry[0])  || isNaN(latLongAry[1]) ) {
      this.dialogService.alert('Latitude,Longitude format must be: number,number');
      return;
    }

    eventData['lat'] = parseFloat(latLongAry[0]);
    eventData['lng'] = parseFloat(latLongAry[1]);

    delete eventData['latLongitude'];


    eventData['type'] = this.type;



    //--- Incident specific
    if (this.type == 'Incident') {   

      let date: Date;
      if (! this.form.value.currentTime) {
        date = new Date(eventData['dateTime'].replace(/T/, ' ')); 
      } else {
        date = new Date();  
      }

      // fix the time 
      eventData['time'] = date.toString();
      delete eventData['dateTime'];
      delete eventData['currentTime'];

      eventData['hour'] = date.toTimeString().substring(0,5);
	      
	  eventData['lane'] = this.form.value.lane;

      if (this.subType == "Crash") {
    	  eventData['damage'] = this.form.value.damage;
    	  eventData['crashType'] = this.form.value.crashType;
      }


      // ensure we use previous time on edit
      this.form.value.currentTime = false;
      this.form.value.dateTime = date.toString();

    } 


    //--- Event specific
    if (this.type == 'Event') {  

      // save with full date format
      if (eventData['startTime1']) {
        // the current format is short (2016-09-27T14:55)
        eventData['startTime'] = eventData['startTime1'].substring(11); // just hour:min
        let startDate = new Date(eventData['startTime1'].replace(/T/, ' ')); 
        eventData['startTime1'] = startDate.toString(); // keep the full format in DB
        this.form.value.startTime1 = startDate.toString(); // and for edit
      }
      if (eventData['endTime1']) {
        // the current format is short (2016-09-27T14:55)
        eventData['endTime'] = eventData['endTime1'].substring(11); // just hour:min
        let endDate = new Date(eventData['endTime1'].replace(/T/, ' ')); 
        eventData['endTime1'] = endDate.toString(); // keep the full format in DB
        this.form.value.endTime1 = endDate.toString(); // and for edit
      }

    } 



    // add icon for backwared comptability
    let icon = "";
    switch (this.subType) {
    	case "Crash":
    		icon = 'crash-major';
    		break;
    	
    	case "Construction":
    		icon = "construction"
    		break;
    	
    	case "Incident":
    		icon = "incident"
    		break;
    	
    	case "Event":
    		switch (eventData['eventType']) {
    			case "concert":
    			case "conference":
    			case "crowd":
    			case "show":
    				icon = eventData['eventType']
    				break;
     			case "sport":
    				icon = "stadium"
    				break;
    			
    			default:
    				// code...
    				break;
    		}
    		break;
    	
    	default:
    		// code...
    		break;
    }
    eventData['icon'] = icon;




    // store data for future edit 
    eventData['eventEditCfg'] = this.qs.getFilledQuestionsCfg(this.subType, this.form.value);

    // if (1) { 
    // 	console.log("cfg=", eventData);
    // 	return;
    // 	}


    // final steps
    
    if (this.isAddMode) { 


      // insert it to the DB
      this.els.addEvent(eventData);


      // avoid re-submit - clear the form
      this.questions = this.qs.getQuestions(this.type); 
      this.form = this.qcs.toFormGroup(this.questions);     
  
    }
    else { // Edit/Copy

      // insert it to the DB
      if (this.copyMode) {
        this.els.addEvent(eventData); // Like Add
      } else {
      	this.els.updateEvent(this.eventKey, eventData, this.oldEvent);

      }

    	this.readyForEdit = null; // will clear the page by *ngIf  
   
   		this.router.navigate(['/show']);
   	}
   	
  }




  onCancel() {


    if (!this.isAddMode) { // edit/copy

      this.readyForEdit = null; // will clear the page by *ngIf  
   
    } else {

      this.questions = this.getQuestions(this.type); 
      this.form = this.qcs.toFormGroup(this.questions);  

    }


  }
    

  setFormCoords(lat, lng) {

   		if (lat && lng && this.form && this.form.controls) {
   			let coords1 = lat.toFixed(6) + ", " + lng.toFixed(6);

		    this.form.controls['latLongitude'].setValue(coords1);
   		}
  }


  // this method is used to set the coords by external push (from main map)
  ngOnChanges() {

  		this.setFormCoords(this.lat, this.lng);

   }


   // this method is used to set coords based on intermna small map
   // TODO: remove me if you don't want to support click on  small map
   mapClicked(event) { 

  		this.setFormCoords(event.coords.lat, event.coords.lng);
  }


  getDate() {

  	if (! this.form.value.dateTime) {
  		return "";
  	}

  	let d = new Date(this.form.value.dateTime.replace(/T/, ' ')); 

  	return d.toLocaleString();
  }

  getDescription() {

  	let res = "";

  	let crashType = (this.form.value.crashType == "crash-minor") ? "Minor Crash" : "Major Crash"; 

  	let damage = "";

  	switch (this.form.value.damage) {
  		case "property_damage":
  			damage = "with no injeries"
  			break;
   		case "injeries":
  			damage = "with injeries"
  			break;
   		case "fatalities":
  			damage = "with fatalities"
  			break;
  		
  		default:
  			// code...
  			break;
  	}

  	let event = "";

  	switch (this.form.value.eventType) {
  		case "concert":
  			event = "Concert"
  			break;
   		case "conference":
  			event = "Conference is scheduled"
  			break;
   		case "crowd":
  			event = "Crowd reported"
  			break;
  		
   		case "show":
  			event = "Show"
  			break;
  		
   		case "sport":
  			event = "Sport event at the stadium"
  			break;
  		
  		default:
  			// code...
  			break;
  	}


  	switch (this.subType) {
  		case "Crash":
  			res = crashType + " on " + this.form.value.lane + " lane " + damage; 
  			break;
  		
  		case "Construction":
  			res = "Construction on " + this.form.value.lane + " lane"; 
  			break;
  		
  		case "Incident":
  			res = "Road on " + this.form.value.lane + " lane"; 
  			break;
  		
  		case "Event":
  			res = event; 
  			break;
  		
  		default:
  			// code...
  			break;
  	}



  	return res;

  }

  
}
