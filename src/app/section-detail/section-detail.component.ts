import {Component, OnInit, Input} from '@angular/core';
import { CfgService } from '../cfg.service';

@Component({
  selector: 'app-section-detail',
  templateUrl: './section-detail.component.html',
  styleUrls: ['./section-detail.component.css']
})
export class SectionDetailComponent implements OnInit {

  @Input() section;
  hour: number = 0;
  startHour = 0; // For ayalon we temporartly need to substrcut 
  weather;

  public activeIndex = 0;

  constructor(private cfgs: CfgService) {

  }


  ngOnInit() {
    this.hour = (new Date()).getHours();
  
    if (this.cfgs.getSite() == "ayalon") {
      this.startHour = 8;
    }
  }

  public tabChanged({index}) {
    this.activeIndex = index;
  }
}
