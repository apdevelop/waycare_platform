import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {RouterModule} from "@angular/router";


// Global components:
import {AppComponent} from './app.component';
import {AuthComponent} from './auth/auth.component';

// Auth
import {AUTH_PROVIDERS} from 'angular2-jwt';
import { AuthGuard } from "./auth/auth.guard";

// Firebase
import {AngularFireModule} from 'angularfire2';

// Material design components
import {MdlModule} from 'angular2-mdl';

// maps
import {AgmCoreModule} from 'angular2-google-maps/core';

// pagination
import {Ng2PaginationModule} from 'ng2-pagination'; 
import { FilterPipe } from './show/filter.pipe';


// user components
import {HeaderComponent} from './header/header.component';
import {DecisionComponent} from './decision/decision.component';
import {AccidentComponent} from './accident/accident.component';

// services
import {Auth} from "./auth/auth.service";
import {firebaseConfig, googleConfig} from "./config";
import {AppService} from "./app.service";
import {MomentModule} from "angular2-moment/index";
import {AccidentService} from "./accident.service";
import {SectionComponent} from './section/section.component';
import {IncidentComponent} from './incident/incident.component';
import {ChartsModule} from "ng2-charts/ng2-charts";
import {SectionChartComponent} from './section-chart/section-chart.component';
import {MdlSelectModule} from "@angular2-mdl-ext/select";
import {MdlPopoverModule} from "@angular2-mdl-ext/popover";
import {MapService} from "./map.service";
import { SectionDetailComponent } from './section-detail/section-detail.component';
import { WeatherDetailComponent } from './weather-detail/weather-detail.component';
import { IncidentDetailComponent } from './incident-detail/incident-detail.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import {NgsRevealModule} from "ng2-scrollreveal/index";
import { HomeComponent } from './home/home.component';
import { AddComponent } from './add/add.component';
import { ShowComponent } from './show/show.component';
import { CardComponent } from './card/card.component';
import { DynamicFormQuestionComponent } from './dynamic-form/dynamic-form-question.component';
import { QuestionService } from "./dynamic-form/question.service";
import { QuestionControlService } from "./dynamic-form/question-control.service";
import { EventService } from "./event.service";
import { CfgService } from "./cfg.service";
import { NewSectionComponent } from './new-section/new-section.component';
import { PreSectionComponent } from './pre-section/pre-section.component';
import { NewAddComponent } from './new-add/new-add.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    AuthComponent,
    DecisionComponent,
    AccidentComponent,
    SectionComponent,
    IncidentComponent,
    SectionChartComponent,
    SectionDetailComponent,
    WeatherDetailComponent,
    IncidentDetailComponent,
    EventDetailComponent,
    HomeComponent,
    AddComponent,
    ShowComponent,
    CardComponent,
    DynamicFormQuestionComponent,
    FilterPipe,
    NewSectionComponent,
    PreSectionComponent,
    NewAddComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    MdlModule,
    Ng2PaginationModule,
    RouterModule.forRoot([
      {path: '', component: HomeComponent},
      {path: 'accident', component: AccidentComponent, canActivate: [AuthGuard]},
      {path: 'decision', component: DecisionComponent, canActivate: [AuthGuard]},
      {path:'show', component: ShowComponent, canActivate: [AuthGuard]},   
      {path:'add', component: AddComponent, canActivate: [AuthGuard]},   
      {path:'edit', component: AddComponent, canActivate: [AuthGuard]},   
    ]),
    AngularFireModule.initializeApp(firebaseConfig),
    AgmCoreModule.forRoot({
      apiKey: googleConfig.apiKey
    }),
    MomentModule,
    ChartsModule,
    MdlModule,
    MdlSelectModule,
    MdlPopoverModule,
    NgsRevealModule.forRoot(),
    HttpModule
  ],
  providers: [
    AUTH_PROVIDERS,
    Auth,
    AuthGuard,
    AccidentService,
    MapService,
    AppService,
    QuestionControlService,
    QuestionService,
    EventService,
    CfgService,
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
