import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-decision',
  templateUrl: './decision.component.html',
  styleUrls: ['./decision.component.css']
})
export class DecisionComponent implements OnInit {
  center = {
    lat: 39,
    lng: -95
  }
  zoom = 5;

  users = [
    {
      name: 'City of Fort Lauderdale',
      code: 'fll'
    },
    {
      name: 'City of Tampa',
      code: 'tampa'
    },
    {
      name: 'City of Las Vegas',
      code: 'lv'
    },
    {
      name: 'National Road and Safety Authority(IL)',
      code: 'rsa'
    },
    {
      name: 'Ayalon Highway Tel Aviv(IL)',
      code: 'ayalon'
    }
  ]

  disable = true;
  clicked = false;

  constructor() {

  }


  ngOnInit() {
  }

  onClick(){
    this.clicked = true;
  }

  onClick1(){
    this.clicked = false;
  }
}
