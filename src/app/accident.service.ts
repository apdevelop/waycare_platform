import {Injectable} from '@angular/core';
import {AngularFire} from "angularfire2/index";
import { CfgService } from './cfg.service';

interface Prediction {
  hour:number;
  result:number;
}

interface Section {
  name:string;
  prediction:Prediction[];
}

@Injectable()
export class AccidentService {

  colors = [
    {
      value: 0.0,
      title: 'Low',
      color: '#00EE76',
    },
    {
      value: 0.1,
      title: 'Low',
      color: '#00EE76',
    },
    {
      value: 0.2,
      title: 'Low',
      color: '#00CD66',
    },
    {
      value: 0.3,
      title: 'Low',
      color: '#008B45',
    },
    {
      value: 0.4,
      title: 'Low - Moderate',
      color: '#FFFF00',
    },
    {
      value: 0.5,
      title: 'Moderate',
      color: '#EEEE00',
    },
    {
      value: 0.6,
      title: 'Moderate-High',
      color: '#F37373',
    },
    {
      value: 0.7,
      title: 'Moderate - High',
      color: '#EA5050',
    },
    {
      value: 0.8,
      title: 'High',
      color: '#EB1D1D',
    },
    {
      value: 0.9,
      title: 'High',
      color: '#EB0B0B',
    },
    {
      value: 1.0,
      title: 'Very High',
      color: '#F60000',
    }
  ]


  sections:any[] = [];

  incidents:any[] = []; // array where incident[dbkey] = actual-incident-at-dbkey 
  events:any[] = [];    // array where events[dbkey] = actual-event-at-dbkey 




  constructor(private af:AngularFire, private cfgs: CfgService) {

    this.initDbItems();

  }


  initDbItems() {

    this.sections = [];
    this.incidents = [];
    this.events = [];

    this.initEvents();

    this.initIncidents();

    this.initSections();

  }


  initSections() {

    // Important: In order to enable auto-refresh of data in section-detail
    // we must keep the this.sections stable (in terms of pointrs to elements) 
    // solution: on second update we copy data and not override the entire sections[i]

    let curHour = new Date().getHours();
    let hour = curHour - curHour % 2;
    this.af.database.list(this.cfgs.getDbPath() + '/sections', {preserveSnapshot: true})
      .subscribe(snapshots=> {
        
        for (let i = 0; i < snapshots.length; i++) {

          let snapshot = snapshots[i];

          let section = {
            'id': snapshot.val().id,
            'name': snapshot.val().name,
            'result': this.getResult(snapshot.val().prediction, hour),
            'prediction': this.getPrediction(snapshot.val().prediction, hour),
            'incidents': this.getElementInArray(snapshot.val().incidents, "incidents"),
            'weather': snapshot.val().weather, 
            'events': this.getElementInArray(snapshot.val().events, "events"),
            'startPoint': snapshot.val().startPoint,
            'endPoint': snapshot.val().endPoint,
            'path': snapshot.val().path,
            'style': this.getStyle(snapshot.val().prediction),
          };

          if (! this.sections[i]) { // First time
            this.sections[i] = section;
          } else {
            for (var key in section) {
               this.sections[i][key] = section[key];
            }
          }

        }

        //console.log("Done loading section ", this.sections);

      })
  }


  // fills the incidents arary - array is used by getElementInArray
  initIncidents() {
    this.af.database.list(this.cfgs.getDbPath() + '/incidents', {preserveSnapshot: true})
      .subscribe(snapshots=> {
        snapshots.forEach(snapshot => {
          let incident = snapshot.val();
          incident['dbkey'] = snapshot.key; // needed for Edit
          //console.log("key= ", snapshot.key, " val=", snapshot.val());
          this.incidents[snapshot.key] = incident;
        });
        //console.log("Done loading incidents ", this.incidents)
      });
  }


  // fills the events arary - arary is used by getElementInArray
  initEvents() {
    this.af.database.list(this.cfgs.getDbPath() + '/events', {preserveSnapshot: true})
      .subscribe(snapshots=> {
        snapshots.forEach(snapshot => {
          let event = snapshot.val();
          event['dbkey'] = snapshot.key; // needed for Edit
          //console.log("key= ", snapshot.key, " val=", snapshot.val());
          this.events[snapshot.key] = event;
        });
        //console.log("Done loading events ", this.events);
      });
  }



  // gets list of references to events/incidents and return arary of objects (instead of referrences)
  getElementInArray(dbList, type) {
    let res = [];
    for (var e in dbList) {
      if (e) {
        if (type == "incidents") {
          if (this.incidents[e])
            res.push(this.incidents[e]);
        }
         if (type == "events") {
           if (this.events[e])
              res.push(this.events[e]);
        }
      }
    } 
    //console.log("Done getElementInArray type=", type, "res=", res);
    return res;
  }



  getSections():any {
    return this.sections;
  }

  getResult(prediction:any, hour) {
    for (var i = 0; i < prediction.length; i++) {
      if (prediction[i].hour == hour) {
        return prediction[i].result;
      }
    }
    return -1;
  }



  // for slider - contains all hours in array
  getStyle(prediction:any) {
    var res = [];
    for (var i = 0; i < prediction.length; i++) {
        let color = {   // default
          value: 0.0,
          title: 'Low',
          color: '#00EE76',
        };

        for (let j = 0; j < this.colors.length; j++) {
          if (this.colors[j].value == prediction[i].result) {
            color = this.colors[j];
          }
        }

      res[prediction[i].hour] = color;  
    }
    return res;
  }




  getPrediction(prediction:any, hour) {
    let prediction_arr = [];
    for (let i = 0; i < prediction.length; i++) {
      prediction_arr.push({
        'hour': prediction[i].hour,
        'result': prediction[i].result
      });
    }
    return prediction_arr;
  }




  getWeather(weather:any) {
    console.log("weather ", weather)
    let weather_arr = [];
    for (let j = 0; j < weather.length; j++) {
      weather_arr.push({
        'hour': weather[j].hour,
        'temp': weather[j].temp,
        'wind': weather[j].wind,
        'windDirection': weather[j].windDirection,
        'conditions': weather[j].conditions,
        'conditionsDirection': weather[j].conditionsIcon
      })
    }
    return weather_arr;
  }

}
