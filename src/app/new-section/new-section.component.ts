import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-new-section',
  templateUrl: './new-section.component.html',
  styleUrls: ['./new-section.component.css']
})
export class NewSectionComponent implements OnInit {

  @Input() section;
  @Input() traffic;
  @Input() weather;
  @Input() incidents;

  @Input() sliderTime;

  constructor() { }

  ngOnInit() {
  }

  // for slider
  getStyle() {

    let defStyle = {
      value: 0.1,
      color: '#E8E8E8',
      title: 'Not selected'
    };

    if (this.section.style) {
      if (this.section.style[this.sliderTime]) {
        return(this.section.style[this.sliderTime]);
      }
    }
    return(defStyle);
  }


  editEvent(event) {

    console.log("section card action editEvent:", event);

    // Edit-TODO
    // This is what we should call:
    //   this.els.setEventUnderEdit(event);
    // + we need to "tell" panel to display edit tab - how???
    // Note: event is incident/event as we provide from accident-service.getIncidents()
    //
    // Note: edit tab is just like Add tab whil the fields will be 
    //     based on the event we just called with setEventUnderEdit(event)


  }


}
