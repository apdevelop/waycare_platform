import {Component, OnInit, Input} from '@angular/core';

@Component({
  selector: 'app-section',
  templateUrl: './section.component.html',
  styleUrls: ['./section.component.css']
})
export class SectionComponent implements OnInit {

  @Input() section;

  @Input() sliderTime;

  latestIncident = {
    type: '',
    time: ''
  };


  constructor() { }

  ngOnInit() {
  }


  // for slider
  getStyle() {

    let defStyle = {
        value: 0.1,
        color: '#E8E8E8',
        title: 'Not selected'
    };

     if (this.section.style) {
       if (this.section.style[this.sliderTime]) {
         return(this.section.style[this.sliderTime]);
       }
     }
     return(defStyle);
  }

}
