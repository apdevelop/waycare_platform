import { Injectable }   from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';

import { QuestionBase } from './question-base';
import { MultiCheckboxQuestion} from './question-multiCheckbox';
import { TextboxQuestion} from './question-textbox';


@Injectable()
export class QuestionControlService {
  constructor() { }

  group: any = {};

  toFormGroup(questions: QuestionBase<any>[] ) {
    //let group: any = {};

    questions.forEach(question => {

		    this.setupFormControl(question);

    });

    return new FormGroup(this.group);
  }



  setupFormControl(question: QuestionBase<any>) {

  	
    if (question.controlType == 'multiCheckbox') {
    	// MultiCheckBox is a special case since there is no native support to put them 
    	// under one FormControl as with select. 

    	let q: any = question;
    	 q.options.forEach(opt => {
   		 	  	 		this.group[q.key + '__' + opt.key] = new FormControl(opt.value || false);

                if (opt.altInput) {
                  this.group[q.key + '__' + opt.key + '__altInput' ] = new FormControl("");
                }

    	 			}
    	 		);

    } else {

      if (question.controlType == 'textbox') {
        let q: any = question;

           if ((q.type == "datetime-local") && (q.value != "")) {

             // in the past we used to keep moment format in the DB so we don't need to handle it
             if (q.value.length > 17) { 

                //console.log("d1:", question);
                var date2 = new Date(q.value);
                var moment = require('moment');
                question.value = moment(date2).format().substring(0,16);
                //console.log("d2:", question);
              }

            }
       }


    	this.group[question.key] = 
    		question.required ? new FormControl(question.value || '', Validators.required)
                                              : new FormControl(question.value || '');
    }

  }



}
