export class QuestionBase<T>{
  value: T;
  key: string;
  label: string;
  required: boolean;
  disabled: boolean;
  useKeyAsValue: boolean;
  order: number;
  hideCond: string;
  icon: string;
  controlType: string;
  constructor(options: {
      value?: T,
      key?: string,
      label?: string,
      required?: boolean,
      disabled?: boolean,
      useKeyAsValue?: boolean,
      order?: number,
      hideCond?: string,
      icon?: string,
      controlType?: string
    } = {}) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || '';
    this.hideCond = options.hideCond || '';
    this.icon = options.icon || '';
    this.required = !!options.required;
    this.disabled = !!options.disabled;
    this.useKeyAsValue = !!options.useKeyAsValue;
    this.order = options.order === undefined ? 1 : options.order;
    this.controlType = options.controlType || '';
  }
}
