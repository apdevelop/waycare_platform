import { Component, Input } from '@angular/core';
import { FormGroup}        from '@angular/forms';
import { QuestionBase }     from './question-base';
@Component({
  //Avi//moduleId: module.id,
  selector: 'df-question',
  templateUrl: 'dynamic-form-question.component.html',
  styleUrls: ['dynamic-form-question.component.css'],

})
export class DynamicFormQuestionComponent {
  @Input() question: QuestionBase<any>;
  @Input() form: FormGroup;
  get isValid() { return this.form.controls[this.question.key].valid; }

  // workaround since the default datetime placeholder overlaps the label
  // the workaround ensures we do see the label (in float state) 
  private dateTimePlaceholder = "-";

  private mystr = "fa fa-id-card-o";

}
