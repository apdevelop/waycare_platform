import { Injectable }       from '@angular/core';
import { DropdownQuestion } from './question-dropdown';
import { QuestionBase }     from './question-base';
import { TextboxQuestion }  from './question-textbox';
import { RadioQuestion }  from './question-radio';
import { CheckboxQuestion }  from './question-checkbox';
import { MultiCheckboxQuestion }  from './question-multiCheckbox';
import { siteConfig } from '../config';
import { CfgService } from '../cfg.service';


@Injectable()
export class QuestionService {

  resData: {} = {};

  resCfg: {} = {};

  formConfig;

  constructor(private cfgs: CfgService) {

    this.formConfig =  siteConfig[this.cfgs.getSite()].formConfig;

  };

  getQuestion(q: {}) {

    //console.log("q=", JSON.stringify(q));  

    let newQ: QuestionBase<any>;

    switch (q['type']) {

      case 'Textbox':
          newQ = new TextboxQuestion(q['cfg']);
        break;

      case 'Radio':
          newQ = new RadioQuestion(q['cfg']);

        break; 

      case 'Checkbox':
          newQ = new CheckboxQuestion(q['cfg']);
        break; 

      case 'MultiCheckbox':
          newQ = new MultiCheckboxQuestion(q['cfg']);
         
        break; 

      case 'Dropdown':
          newQ = new DropdownQuestion(q['cfg']);
          
        break; 

    }

    

    return newQ;

  }


  getQuestionsForCfg(type: string, cfg) {

    let res = [];

    // comon questions
    let common: {} = cfg['Common'];

    for (var q in common){
        if (! common[q]['cfg']['disabled'])
          res.push(this.getQuestion(common[q]));       
    }

    // type specific questions
    let specific: {} = cfg[type];

    for (var q in specific){
      
        if (! specific[q]['cfg']['disabled'])
          res.push(this.getQuestion(specific[q])); 
    }

    // sorting
    res.sort(function(a, b){
          if (a.order < b.order) return -1;
          if (a.order > b.order) return 1;
          return 0;
        });

    //console.log("res=", res);

    return res;

  }






  getCfg() {
    return this.formConfig;
  }

  getQuestions(type: string) {

    return this.getQuestionsForCfg(type, this.formConfig);
   
  }




  getTypes() {

    let res = [];

    for (var t in this.formConfig){
        if (t != 'Common') {
          res.push(t);
        }
    }    

    return res;

  }


//=================  form answers... ===================================


  // returns json that uses the form key on left side and labels on right side
  // that is, the answer is in nice format for display


  getFormAnswer(q: {}, formValue: {}) {

    //console.log("q=", JSON.stringify(q));  

    let cfg;
    let keyValueMap = {};

    switch (q['type']) {

      case 'Textbox':
          cfg = q['cfg'];
          this.resData[cfg['key']] = formValue[cfg['key']] || "";
        break;

      case 'Radio':
          cfg = q['cfg'];

          if (cfg['useKeyAsValue']) {
            this.resData[cfg['key']] = formValue[cfg['key']];
          }
          else {
            for (var opt in q['cfg']['options']) { 
              keyValueMap[q['cfg']['options'][opt]['key']] = q['cfg']['options'][opt]['value']; 
            }
            this.resData[cfg['key']] = keyValueMap[formValue[cfg['key']]];
          }

        break; 

      case 'Checkbox':
          cfg = q['cfg'];
          this.resData[cfg['key']] = formValue[cfg['key']];

        break; 

      case 'MultiCheckbox':
          cfg = q['cfg'];
          

          // build prity value
          let str = '';
          let first = true;
          for (var opt in q['cfg']['options']) { 

            if (formValue[cfg['key'] + '__' + cfg['options'][opt]['key']]) {

              let newItem = cfg['options'][opt]['text'];

              if (cfg['useKeyAsValue']) {
                 newItem = cfg['options'][opt]['key'];
              }

              // special case of altInput
              if (cfg['options'][opt]['altInput']) {
                let name = cfg['key'] + '__' + cfg['options'][opt]['key'] + '__altInput';
                if (formValue[name]) { // might be null
                  newItem = newItem + ": " + formValue[name];
                }         
              }

              if (first) {
                first = false;
                str = newItem;
              } else {
                str = str + ", " + newItem;
              }
            }
          }

          this.resData[cfg['key']] = str;
            

        break; 

      case 'Dropdown':
          cfg = q['cfg'];


          if (cfg['useKeyAsValue']) {
            this.resData[cfg['key']] = formValue[cfg['key']] || '';
          }
          else {
            for (var opt in q['cfg']['options']) { 
              keyValueMap[q['cfg']['options'][opt]['key']] = q['cfg']['options'][opt]['value']; 
            }

            this.resData[cfg['key']] = keyValueMap[formValue[cfg['key']]] || '';
          }

        break; 

    }    
  }



  getFormAnswers(type: string, formValue: {}) {

    //console.log('getFormAnswers ', type, formValue);

    this.resData = {}; // clear global 


    // comon questions
    let common: {} = this.formConfig['Common'];

    for (var q in common){
        this.getFormAnswer(common[q], formValue);
    }

    // type specific questions
    let specific: {} = this.formConfig[type];

    for (var q in specific){
        this.getFormAnswer(specific[q], formValue);
    }

    return this.resData;

  }


  //================================ question-cfg for re-edit ========
  // provide modified version of the form_config where user's answers are 
  // saved as in value field instead of default


  getFilledQuestionCfg(q: {}, formValue: {}, cfgEntry: string, category: string) {


    let cfg;

    switch (q['type']) {

      case 'Textbox':
      case 'Radio':
      case 'Checkbox':
      case 'Dropdown':

          cfg = q['cfg'];

          this.resCfg[category][cfgEntry]['cfg']['value'] = formValue[cfg['key']];


        break; 

      case 'MultiCheckbox':
          cfg = q['cfg'];
          

          // build prety value
          for (var opt in q['cfg']['options']) { 

            if (formValue[cfg['key'] + '__' + cfg['options'][opt]['key']]) {

              // set the checked items
              this.resCfg[category][cfgEntry]['cfg']['options'][opt]['value'] = true;

              // special case of altInput
              if (cfg['options'][opt]['altInput']) {
                let name = cfg['key'] + '__' + cfg['options'][opt]['key'] + '__altInput';
                if (formValue[name]) { // might be null
                  // TODO
                }         
              }

            }
          }

            
        break; 

    }    
  }


  getFilledQuestionsCfg(type: string, formValue: {}) {


    this.resData = {}; // clear global 
    
    this.resCfg = JSON.parse(JSON.stringify(this.formConfig)); // (must clone)

    // comon questions
    let common: {} = this.formConfig['Common'];

    for (var q in common){
        this.getFilledQuestionCfg(common[q], formValue, q, 'Common');
    }

    // type specific questions
    let specific: {} = this.formConfig[type];

    for (var q in specific){
        this.getFilledQuestionCfg(specific[q], formValue, q, type);
    }

    return this.resCfg;

  }




}

