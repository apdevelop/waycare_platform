import { Component, OnInit } from '@angular/core';
import {Auth} from "../auth/auth.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private auth: Auth) { }

  ngOnInit() {

  		// close the drawer after click
		document.querySelector('.mdl-layout__drawer').addEventListener('click', function () {
  			document.querySelector('.mdl-layout__obfuscator').classList.remove('is-visible');
  			this.classList.remove('is-visible');
		}, false);

  }


}
