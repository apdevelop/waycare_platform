import { Injectable} from '@angular/core';

import {AngularFire, FirebaseListObservable, FirebaseObjectObservable} from "angularfire2";

import { QuestionService } from './dynamic-form/question.service';
import { CfgService } from './cfg.service';

import { appDefaults } from "./config";

import { MdlDialogService } from "angular2-mdl";

import 'rxjs/add/operator/map';
import 'rxjs/add/operator/take';


@Injectable() export class EventService {


  events:any[] = [];
  incidents:any[] = [];

  private eventUnderEdit;


  constructor(private qs: QuestionService, private af: AngularFire, 
      private cfgs: CfgService,
      private dialogService: MdlDialogService) {
  
     this.initDbItems();


  }



  // inits incidents and events based on multiple days range
  initDbItems() {

    //  find dates
    this.af.database.list(this.cfgs.getDbPathDates(), 
          {preserveSnapshot: true,
           query: {
              orderByKey: true,
              startAt: this.cfgs.getStartDate(),
              endAt: this.cfgs.getEndDate(),
            }
          }
          )
      .subscribe(snapshots=> { // get data per date

        let incidents = [];
        let events = [];


        snapshots.forEach(snapshot => {

            let dateEntry = snapshot.val();

            for (var k in dateEntry.incidents){
              let entry = dateEntry.incidents[k];
              entry['dbkey'] = k; // ref for edit/delete
              incidents.push(entry);
            }

            for (var k in dateEntry.events){
              let entry = dateEntry.events[k];
              entry['dbkey'] = k; // ref for edit/delete
              events.push(entry);
            }

        } );

        this.incidents = incidents.slice(0); // clone to trigger a update in usage
        this.events = events.slice(0);

      });
  }



  toLowerPlural(type) {
    return ( type.toLowerCase() + "s" );
  }


  // return the date for which the event is/should-be stored in the DB
  getEventDbDate(event) {

    let dbDate;

    if (event['type'] == 'Event') {  

      if (event['startTime1']) {
        if (event['startTime1'].length > 17) {
          dbDate = new Date(event['startTime1']); 
        } else { // TODO: remove once all DB using full format
          dbDate = new Date(event['startTime1'] .replace(/T/, ' ')); 
        }
      } else {
        dbDate = new Date();
      }

    } else { // Incident

      dbDate = event['time'];

    } 

    return dbDate;

  }



  // add the event in  places
  // We first write to:
  //       incidents/<incident-json>
  // and then for each affected section we set 
  //       sections/<section>/incidents/<incident-json>
  addEvent(event: {}) {


    let type = event['type'];
    let dbDate = this.getEventDbDate(event);

    let newEventRef = this.af.database.list(this.cfgs.getDbPathForDate(dbDate) + "/" + type.toLowerCase() + "s").push(event);
    //console.log("newRef.name=", newEventRef.key);

    // now add also to each affected section
    let sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let eventRef = {valid : true};
      this.af.database.object(this.cfgs.getDbPathForDate(dbDate) + '/sections/'  + sections[i] + "/" + 
        type.toLowerCase() + "s" + "/" + newEventRef.key).set(eventRef); 

    }

  }

  
  deleteEvent(event: {}) {

    // Need to delete it's reference in all sections and then to delete the event itself
    // Enh: try to do both as one atomic operation

    //console.log("in event.service deleteEvent", event);
    //console.log("in event.service deleteEvent", event['dbkey']);

    let dbDate = this.getEventDbDate(event);

    let key = event['dbkey'];
    if (! key) {
      let result = this.dialogService.alert("Can't delete w/o valid dbkey");
      //result.subscribe( () => console.log('alert closed') );
      return;
    }

    let type = event['type'];
    if (! type) {
      let result = this.dialogService.alert("Can't delete w/o valid type attribute");
      //result.subscribe( () => console.log('alert closed') ); 
      return;
    }


    // delete ref in each affected section
    let sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let ref = this.af.database.object(this.cfgs.getDbPathForDate(dbDate) + '/sections/'  + sections[i] + "/" + 
        type.toLowerCase() + "s" + "/" + key); 

      ref.remove();
    }

    // delete the event itself
    let ref = this.af.database.object(this.cfgs.getDbPathForDate(dbDate) + "/" + type.toLowerCase() + "s" + "/" + key);

    ref.remove();

  }
  


  // update same entry in the db 
  // 1. override the event in it's dbkey location
  // 2. update aaffected sections (some may need to be updated)
  updateEvent(key, event, oldEvent) {

    // Special case: if dbDate of old and new are not the same, just do delete(old) and add(new)
    if (this.getEventDbDate(event) != this.getEventDbDate(oldEvent)) {

      this.deleteEvent(oldEvent);
      this.addEvent(event);
      return;

    }


    // 1. override the event in it's dbkey location


    if (! key) {
      let result = this.dialogService.alert("Can't update w/o valid dbkey");
      return;
    }

    let type = event['type'];
    if (! type) {
      let result = this.dialogService.alert("Can't update w/o valid type attribute");
      return;
    }

    let dbDate = this.getEventDbDate(event);

    // update the event itself
    this.af.database.object(this.cfgs.getDbPathForDate(dbDate) + "/" + type.toLowerCase() + "s" + "/" + key).set(event); 




    // 2. update aaffected sections (some may need to be updated)

    // in order to trigger refresh on platform, we prefer to delete & insert the references in the sections

    // delete ref in each affected section
    //let sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    let sections = oldEvent['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let ref = this.af.database.object(this.cfgs.getDbPathForDate(dbDate) + '/sections/'  + sections[i] + "/" + 
        type.toLowerCase() + "s" + "/" + key); 

      ref.remove();
    }

    // insert for each section
    sections = event['affectedSections'].replace(/\s+/g, '').split(",");
    for (var i = 0, len = sections.length; i < len; i++) {

      if (sections[i] == "") continue;

      let eventRef = {valid : true};
      this.af.database.object(this.cfgs.getDbPathForDate(dbDate) + '/sections/'  + sections[i] + "/" + 
        type.toLowerCase() + "s" + "/" + key).set(eventRef); 

    }


  }




  likeEvent(event: {}, positive: boolean) {


    let dbDate = this.getEventDbDate(event);

    let key = event['dbkey'];
    if (! key) {
      let result = this.dialogService.alert("Can't perform w/o valid dbkey");
      return;
    }

    let type = event['type'];
    if (! type) {
      let result = this.dialogService.alert("Can't perform w/o valid type attribute");
      return;
    }


    let path = this.cfgs.getDbPathForDate(dbDate) + "/" + 
      type.toLowerCase() + "s" + "/" + key + "/like";
   
    // in case path doesn't exist, it'll be created with value=0
    this.af.database.object(path , { preserveSnapshot: true }).take(1).
      subscribe(counter => { 
          let cnt = counter.val();
          if (cnt == null) { cnt = 0; }
          positive ? cnt++ : cnt--;
          this.af.database.object(path).set(cnt);
        });


  }
  




  setEventUnderEdit(event) {
    this.eventUnderEdit = event;
  }

  getEventUnderEdit() {
    return this.eventUnderEdit;
  }


  clearEventUnderEdit() {
    this.eventUnderEdit = null;
  }



}