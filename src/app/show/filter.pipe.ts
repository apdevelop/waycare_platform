// converts the async data into simple array
// filters based on regexp

import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  filterBy: string;

  transform(arr, filterBy) {

     
      // get rid of extra space after ':'
      let value2 = filterBy.replace(/: /, ":");
      this.filterBy = value2;

  	// need to handle null case in order to support obvervables 
  	if (arr != null) {

      var filteredArr = [];

      for (var i = 0; i < arr.length; i++) { 
        if (this.filter(arr[i])) {
          filteredArr.push(arr[i]);
        }
      }

      return filteredArr; 
    } else {
    	return [];
    }

  }



  // return the values stored inside the event to enable search
  getSearchableString(event) {
    var res = "";
    for (var key in event){
       if (key == "eventEditCfg") continue;
       if (event[key] && (event[key] != "")) {
         res += " "  + key + ":"  + event[key];
       }
    }
    return res;
  }




  filter(event) {

    let passFilter = false;

    if (this.filterBy == "") {
      passFilter = true; // faster - no need to check reexp
    } else {
      var re = new RegExp(this.filterBy, 'i');
      passFilter = (this.getSearchableString(event).match(re) != null);
    }

    return passFilter;
  }





}
