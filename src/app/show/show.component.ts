import { Component, OnInit} from '@angular/core';
import { EventService } from "../event.service";
import { CfgService } from "../cfg.service";
import { QuestionService } from '../dynamic-form/question.service';
import { appDefaults } from "../config";


@Component({
  selector: 'app-show',
  templateUrl: './show.component.html',
  styleUrls: ['./show.component.css']
})
export class ShowComponent implements OnInit {
  
  eventTypes = []; 

  tabTypes = [];

  type: string;

  activeTabIndex = 0;

  filterBy: string = "";


  startDate;
  endDate;


  // array of all items to be paged
  private allItems: any[] = [];


  showPerPage: number;



  constructor(private qs: QuestionService, private els: EventService, private cfgs: CfgService) { 

        this.showPerPage = appDefaults['numEntriesToDisplay'];
        // if we are on small screen -> change to 1 
        if ( window.innerWidth < 500) {
          this.showPerPage = 1;
        }


  }


  ngOnInit() {
   
    this.eventTypes = this.qs.getTypes();

    this.tabTypes = this.eventTypes;

    this.type = this.cfgs.getCurrentType();

    this.activeTabIndex = this.tabTypes.indexOf(this.type);

    this.startDate = this.cfgs.getStartDate();
    this.endDate = this.cfgs.getEndDate();

  }



  tabChanged(tabChangedEvent) {
    this.cfgs.setCurrentType(this.tabTypes[tabChangedEvent.index]);
    this.type = this.tabTypes[tabChangedEvent.index];
  } 



  onSearchKey(value) {
    this.filterBy = value;
  }


  startDateChanged(value) {
    this.cfgs.setStartDate(value);
    this.els.initDbItems();
  }


  endDateChanged(value) {
    this.cfgs.setEndDate(value);
    this.els.initDbItems();
  }









}
