import { Component, Input, ElementRef } from '@angular/core';
import {FormControl} from "@angular/forms";
import { CfgService } from './cfg.service';
import { appDefaults } from "./config";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent  {
	
	site: string = appDefaults['defaultSite'];

	constructor(elm: ElementRef, private cfgs: CfgService) {
		if (elm.nativeElement.getAttribute('site') != "{{site}}") {
			this.site = elm.nativeElement.getAttribute('site');
		} else {
			//console.log("Using default site from app.component: ", this.site);
		}
  		//console.log("site: ", this.site);
  		this.cfgs.setSite(this.site);
	}


}
