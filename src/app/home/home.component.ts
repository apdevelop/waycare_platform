import { Component, OnInit, AfterViewChecked} from '@angular/core';
import {Auth} from "../auth/auth.service";
import {Router} from '@angular/router';


@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  constructor(private auth: Auth, private router: Router) { }


  ngOnInit() {}


  ngAfterViewChecked() {

  	if (this.auth.authenticated()) {
    	this.router.navigate(['/accident']);
    }
   }

}

