
// This service store and provide dynamic configuration information needed by others

import { Injectable } from '@angular/core';
import { siteConfig, appDefaults } from "./config";
import { isDevMode } from '@angular/core';

import {Observable} from 'rxjs/Rx';

@Injectable()
export class CfgService {


	devModeDate = "";
	//devModeDate = "2017-01-24";    // <== developer can override here


	cfgDate: string;
	site: string;

	startDate: string;
	endDate: string;

    private currentType: string;

 	 constructor() { 

 		this.setCfgDate(this.formatDate(new Date()));

 	 	this.currentType = appDefaults['startType'];

 	 	this.setDateRefreshTimer();

 	 }


 	 getTimeToMidnight() {

 	 	let now = new Date();
 	 	let lastCall = now.getTime();
 	 	let midnight = new Date(); 
 	 	midnight.setHours(24,0,0,0);
 	 	return (midnight.getTime() - now.getTime());

 	 }


 	 setDateRefreshTimer() {

 	 	// set a timer to refresh cfgDate every midnight
 	 	// we check every min since timer is not ticking when computer is sleeping
 	 	let now = new Date();
 	 	let lastCall = now.getTime();
 	 	let timeToMidnight = this.getTimeToMidnight();
 
 	 	
 	 	let msMin = 60 * 1000; // we'll check once a min 
 	 	//console.log("In delta=", timeToMidnight);

		let timer = Observable.timer(msMin,msMin);
    		timer.subscribe(t=> {
    			let d1 = new Date();
	    		// console.log("In timer subscribe stage-0", " lastCall=",  lastCall, 
	    		// 	"m sToMidnight=", timeToMidnight, " t=", t, " now=", d1.toString());

    			if ((d1.getTime() - lastCall) > timeToMidnight)  {
	    			//console.log("In timer subscribe stage-1", t, d1.toString());
    		    	this.updateCfgDate();

    		    	// for next time...
    		    	lastCall = d1.getTime();		    	
 	 				timeToMidnight = this.getTimeToMidnight();
    		    }
    	});
 	 }


 	 // return the current cfgDate + currentTime (cfgDate might be override in dev mode)
 	 getDate() {

 	 	let d = (new Date(this.cfgDate).toDateString()) + " " + (new Date().toString().substring(16));

 	 	return new Date(d);

 	 }


 	 // returns date as locale time in YYYY-MM-DD format
	 formatDate(date) {
	    var d = new Date(date),
	        month = '' + (d.getMonth() + 1),
	        day = '' + d.getDate(),
	        year = d.getFullYear();

	    if (month.length < 2) month = '0' + month;
	    if (day.length < 2) day = '0' + day;

	    return [year, month, day].join('-');
	}
 	 


	  getDbPathDates() {
    	return ( "sites/" + this.site + "/dates");
	  }


	  getDbPath() {

	    //console.log('getDbPath1: ' + "sites/" + this.site + "/dates/" + this.cfgDate);

    	return ( "sites/" + this.site + "/dates/" + this.cfgDate );
    	
	  }


	  getDbPathForDate(dbDate) {

	    //console.log('getDbPath2: ', dbDate);


	  	var myDate = this.formatDate(dbDate);

	    //console.log('getDbPath3: ' + "sites/" + this.site + "/dates/" + myDate);

    	return ( "sites/" + this.site + "/dates/" + myDate );
    	
	  }


	  setSite(site){

	  	this.site = site;

	  	this.updateCfgDate(); // might be diffrent based on d-day

	  }
	  
	  getSite() {
	  	return this.site;
	  }


	  // will set the date based on all options (d-day, dev-mode, now)
	  updateCfgDate() {

 	 	if (siteConfig[this.site].dDay) {
 	 		this.setCfgDate(siteConfig[this.site].dDay);
 	 		return;
 	 	}

 	 	if (isDevMode() && (this.devModeDate != "")) {
 	 		this.setCfgDate(this.devModeDate);
 	 		return
 		}

 		this.setCfgDate(this.formatDate(new Date()));
	
	  }


	  setCfgDate(date){
		//console.log("setCfgDate:", date);

	  	this.cfgDate = date;

 	 	this.startDate = this.endDate = this.cfgDate;


	  }

	  getCfgDate() {
	  	return this.cfgDate;
	  }



	  setStartDate(date){
	  	this.startDate = date;
	  }

	  getStartDate() {
	  	return this.startDate;
	  }

	  setEndDate(date){
	  	this.endDate = date;
	  }

	  getEndDate() {
	  	return this.endDate;
	  }



	  setCurrentType(type) {
	    this.currentType = type;
	  }

	  getCurrentType() {
	    return this.currentType;
	  }





}
