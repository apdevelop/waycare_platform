import { Component, OnInit, Input, ViewContainerRef } from '@angular/core';
import { EventService } from "../event.service";
import { MdlDialogService } from "angular2-mdl";
import {Router} from '@angular/router';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.css']
})
export class CardComponent implements OnInit {

	@Input() data: {};

  // mandatory fields
	type: string = "";
	title: string = "";

	myClass: string = 'demo-card-event mdl-card mdl-shadow--2dp';

  fieldsDataList = [];

 
  constructor(private els: EventService, private dialogService: MdlDialogService, 
              private router:Router) {}


  ngOnInit() {

	this.fieldsDataList = []; 

  //console.log("data=", this.data);

    this.type = this.data['type'];
    this.title = this.data['title'];

    this.myClass += ' ' + this.type + '-card';

    // fill in the rest of the data
    for (var key in this.data) {
      if (!key.startsWith("$")) {
          // skip the mandatory fields that were already extracted
         if (key == "title") continue;
         if (key == "type") continue;
         if (key == "eventEditCfg") continue;
         //console.log("key=", key, "   v=" , this.data[key]);
         this.fieldsDataList.push([key, this.data[key]]);
       }
    }

  }




  deleteEvent() {
 
    //console.log("deleteEvent was invoked", this.data['dbkey']);

    let result = this.dialogService.confirm('Are you sure you want to delete this event?', 'No', 'Yes');
    result.subscribe( () => {
        console.log('confirmed');
        this.els.deleteEvent(this.data);
      },
      (err: any) => {
        console.log('declined');
      }
    );
  }


  editEvent(copy:boolean) {
 
    //console.log("editEvent was invoked", this.data['dbkey']);

    this.data['__isCopy'] = copy;

    this.els.setEventUnderEdit(this.data);

    // switch to Edit page 
    this.router.navigate(['/edit']);


  }



  likeEvent(positive: boolean) {
 
    this.els.likeEvent(this.data, positive);
     
  }





}
