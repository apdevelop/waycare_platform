import {Component, OnInit, AfterViewInit} from '@angular/core';
import {AccidentService} from "../accident.service";
import {MapService} from "../map.service";
//import {mapConfig, sliderConfig, siteConfig} from "../config";
import {siteConfig} from "../config";
import { CfgService } from '../cfg.service';
import { EventService } from "../event.service";

@Component({
  selector: 'app-accident',
  templateUrl: './accident.component.html',
  styleUrls: ['./accident.component.css']
})
export class AccidentComponent implements OnInit, AfterViewInit {

  mapConfig;

  center;

  mapClickedLat;
  mapClickedLng;

  displayRoadCond = true;

  zoom;

  sliderTime: number;

  sliderCfg;

  selected_section_index;

  sections
  events
  incidents
  label = '<i class="material-icons">router</i>';
  time: Date;

  selectedSection = {
    name: '',
    result: '',
    prediction: [],
    incidents: [],
    events: [],
    style: [],
    weather:{
      conditions: ''
    },
    path : []
  };

  traffic = {
    speed: '',
    dis: 0,
    breaks: 0,
    risk: "",
    color: ""
  }

  weather = {
    temp: '',
    conditions: '',
    visibility: '',
    wind: '',
    hum: '',
    precip: ''
  }

  r_incidents = [];

  selectedTab = 0;
  selectedAddTab = 0;


  hours = [
    "00:00",
    "04:00",
    "08:00",
    "12:00",
    "16:00",
    "20:00"
  ]

  constructor(private _as: AccidentService, private _ms: MapService, private cfgs: CfgService,
    private els: EventService) {

    let curHour = new Date().getHours();
    let hour = curHour - curHour % 2;
    this.sliderTime = hour;
  }

  ngOnInit() {

    this.time = this.cfgs.getDate();
    this.mapConfig = siteConfig[this.cfgs.getSite()].mapConfig;
    this.center = this.mapConfig.center;
    this.zoom = this.mapConfig.zoom;
    this.sliderCfg = siteConfig[this.cfgs.getSite()].sliderConfig;

  }

  ngAfterViewInit():void {
    this.sections = this._as.getSections();
    this.events = this._ms.getEvents();
    this.incidents = this._ms.getIncidents();
    console.log(this.incidents)
    console.log(this.events)
  }
  onSelect(sectionID){
    let i = 0;
    for (let s of this.sections){
      if (sectionID == s.id){
        i++
        this.selectedSection = s;
      }
    }
    this.selected_section_index = i;
    this.getTraffic();
    this.getWeather();
    this.getIncidents();
  }


    // for slider
    getStyle(section) {

      let defStyle = {
          value: 0.1,
          color: '#E8E8E8',
          title: 'Not selected'
      };

     if (section.style) {
       if (section.style[this.sliderTime]) {
         return(section.style[this.sliderTime]);
       }
     }

     return(defStyle);
  }


  getTraffic(){

    // currently just randomizing
    let r_speed = Math.random() * (60 - 9) + 9;
    let r_dis = Math.floor(Math.random()*12);
    let r_breaks_factor = Math.random() * ( 12 - 8 ) + 8;
    let r_breaks = r_dis * r_breaks_factor;

    let r_dis_result = Math.floor(Math.random()*4);
    console.log(r_dis_result)

    let r_risk = ""
    let r_color = "green"

    switch(r_dis_result){
      case 0:
        // no
        r_speed = Math.random() * (20 - 9) + 9;
        r_dis = Math.floor(Math.random()*4);
        r_breaks = Math.floor(r_dis * r_breaks_factor);

        r_risk = "Low Dis'";
        r_color = "green";

        break;
      case 1:
        // minor
        r_speed = Math.random() * (25 - 15) + 15;
        r_dis = Math.floor(Math.random()*8) + 4;
        r_breaks = Math.floor(r_dis * r_breaks_factor);

        r_risk = "Minor Dis'";
        r_color = "yellowgreen";

        break;
      case 2:
        // moderate
        r_speed = Math.random() * (45 - 20) + 20;
        r_dis = Math.floor(Math.random()*12) + 4;
        r_breaks = Math.floor(r_dis * r_breaks_factor);

        r_risk = "Moderate dis";
        r_color = "orange";

        break;
      case 3:
        // heavy

        r_speed = Math.random() * (60 - 30) + 30;
        r_dis = Math.floor(Math.random()*12) + 8;
        r_breaks = Math.floor(r_dis * r_breaks_factor);

        r_risk = "Heavy dis'";
        r_color = "red";

        break;
    }

    this.traffic = {
      speed: r_speed.toFixed(1),

      dis: r_dis,

      breaks: r_breaks,

      risk: r_risk,
      color: r_color
    }

  }


  getWeather(){

    // currently just randomizing
    let r_visibility = Math.random() * (10 - 6) + 6;
    let r_wind = Math.floor(Math.random() * (16 - 6) + 6);
    let r_hum = Math.random() * ( 30 - 20 ) + 20;

    this.weather = {
      temp: '67',
      conditions: 'cloudy',
      visibility: r_visibility.toFixed(1),
      wind: r_wind + ' WSW',
      hum: r_hum.toFixed(1) + "%",
      precip: '0'
    }

  }

  getIncidents(){

    this.r_incidents = [];

    let r_number = Math.floor(Math.random() * (6 - 2) + 2);

    for (let j = 0; j < r_number; j++){
      let r_incident_type = Math.floor(Math.random() * 5 );
      let incident_type = "";
      let description = "";
      switch (r_incident_type){
        case (0):
          incident_type = "Minor event"
          if (Math.random() > 0.5)
            description = "Car On the shoulders";
          else
            description = "Congestion forming";
          break;
        case (1):
          incident_type = "Minor crash";
          description = "Minor crash on " + this.selectedSection.name;
          break;
        case (2):
          incident_type = "Severe crash";
          if (Math.random() > 0.7)
            if (Math.random() > 0.9){
              description = "Severe crash with fatalities on " + this.selectedSection.name;
            } else {
              description = "Severe crash with injuries on " + this.selectedSection.name;
            }
          else
            description = "Severe crash, property damage only, on " + this.selectedSection.name;
          break;
        case (3):
          incident_type = "Multiple-vehicle collision";
          if (Math.random() > 0.5)
            if (Math.random() > 0.9){
              description = "Severe crash with fatalities on " + this.selectedSection.name;
            } else {
              description = "Severe crash with injuries on " + this.selectedSection.name;
            }
          else
            description = "Severe crash, property damage only, on " + this.selectedSection.name;
          break;
        case (4):
          incident_type = "Focal Point Event";
          if (Math.random() > 0.5)
            description = "Show/Concert near "  + this.selectedSection.name;
          else
            description = "Match/Game near " + this.selectedSection.name;
          break;
        case (5):
          incident_type = "Construction/Malfunction";
          if (Math.random() > 0.5)
            description = "Traffic Light Malfunction on "  + this.selectedSection.name;
          else
            description = "Construction on " + this.selectedSection.name;
          break;
      }

      let minusMin = Math.floor(Math.random()*45)

      let time = new Date(new Date().valueOf() - minusMin*60000);

      let chars = '0123456789ABCDEF'.split('');
      let incident_code = '';
      for(let i=0; i < 6; i++){
        let x = Math.floor(Math.random() * chars.length);
        incident_code += chars[x];
      }
      let new_incident = {
        type: incident_type,
        id: incident_code,
        description: description,
        time: time
      }
      this.r_incidents.push(new_incident);
    }

    console.log(this.r_incidents)
  }


  onLeft(){

    let r_section_index = Math.floor(Math.random() * (this.sections.length));

    this.selectedSection = this.sections[r_section_index];
    this.selected_section_index = r_section_index;
    this.getTraffic();
    this.getWeather();
    this.getIncidents();

  }


  onClickTab(tab){
    this.selectedTab = tab;
  }


  onAddClickTab(tab){
    this.selectedAddTab = tab;
  }



  displayAddCard(){

    this.displayRoadCond = false;

  }

  clearAddCard(){

    this.displayRoadCond = true;

  }



  // just selece the coords
  mapClicked(event) {

    console.log("mapClicked:", event);

    this.mapClickedLat = event.coords.lat;
    this.mapClickedLng = event.coords.lng;


  }


  // invoke Add if not opened + select coords
  mapRightClicked(event) {

    console.log("mapRightClicked:", event);

    this.mapClickedLat = event.coords.lat;
    this.mapClickedLng = event.coords.lng;
    this.displayAddCard();

  }


  editEvent(event) {

    console.log("editEvent:", event);
    this.els.setEventUnderEdit(event);
    this.displayAddCard();

  }


}
