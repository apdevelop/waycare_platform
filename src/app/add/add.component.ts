import { Component, OnInit } from '@angular/core';
import { FormGroup }                 from '@angular/forms';
import { QuestionBase }              from '../dynamic-form/question-base';
import { QuestionControlService }    from '../dynamic-form/question-control.service';
import { QuestionService } from '../dynamic-form/question.service';
 

import { EventService } from "../event.service";
import { Observable } from 'rxjs/Rx';
import { MdlDialogService } from "angular2-mdl";
import { Router } from '@angular/router';
import { CfgService } from '../cfg.service';

import {siteConfig} from "../config";


@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  mapConfig;
  center;
  zoom;

  isAddMode: boolean = false;

  questions: QuestionBase<any>[] = [];

  form: FormGroup;

  type: string = ''; 
 
  eventTypes = []; 

  readyForEdit: boolean;

  copyMode: boolean;

  eventKey; // the key we need to keep using

  oldAffectedSections = [];
  oldEvent;


  constructor(private cfgs: CfgService, private qs: QuestionService, 
    private qcs: QuestionControlService,
    private els: EventService, private dialogService: MdlDialogService,
    private router:Router) { 

   }
  

  // can be changed in Add mode (in edit/copy the type is fixed)
  typeChanged() {
    //console.log('typeChanged to ' + this.type);
    this.cfgs.setCurrentType(this.type); // remember for next time

    this.questions = this.getQuestions(this.qs.getCfg()); 

    this.form = this.qcs.toFormGroup(this.questions);    

  }



  // get the active questions while updating fields if needed
  getQuestions(cfg) {

      
      let qCfg =  JSON.parse(JSON.stringify(cfg)); // clone


      // indicate that this is a copy in the title
      if (this.copyMode) {
        if (qCfg['Common']['titleQ'])
          qCfg['Common']['titleQ']['cfg']['value'] += "(copy)";
      }


      return this.qs.getQuestionsForCfg(this.type, qCfg); 

  }

  
  ngOnInit() {


    this.mapConfig = siteConfig[this.cfgs.getSite()].mapConfig;
    this.center = this.mapConfig.center;
    this.zoom = this.mapConfig.zoom;

  	// are we in 'edit/copy mode or 'add' 
  	this.isAddMode = (this.els.getEventUnderEdit() == null);


  	this.readyForEdit = false;


    if (this.isAddMode) {

      this.type = this.cfgs.getCurrentType(); 

      this.eventTypes = this.qs.getTypes(); 

      this.questions = this.getQuestions(this.qs.getCfg()); 

    }

    else { //--- edit/copy mode


      //--- check that all required data exists

	    let event = this.els.getEventUnderEdit();
	    this.els.clearEventUnderEdit();

	    if (! event ) {
	    	//alert("no event under edit found");
	    	return;
	    }

	    if (! event['eventEditCfg']) {
	    	this.dialogService.alert("Missing eventEditCfg property. can't edit this event.");
	    	return;
	    }

	    this.eventKey = event['dbkey'];

	    if (! this.eventKey ) {
	    	this.dialogService.alert("Missing eventKey property. can't edit this event");
	    	return;
	    }

      this.type = event['type'];

      if (! this.type ) {
        this.dialogService.alert("Missing type property. can't edit this event");
        return;
      }



      this.copyMode = event['__isCopy'];
      delete event['__isCopy'];


	    this.oldAffectedSections = event['affectedSections'];
      this.oldEvent = JSON.parse(JSON.stringify(event)); //clone. required for update

	  	this.readyForEdit = true;

      this.questions = this.getQuestions(event['eventEditCfg']); 


  	} 


    this.form = this.qcs.toFormGroup(this.questions);  


  }




  onSubmit() {

    //console.log("Add, form:", this.form); 


    let eventData = this.qs.getFormAnswers(this.type, this.form.value);


    //--- replace latLongtitde with two entries
    let latLongAry = eventData['latLongitude'].split(",");
    
    // check it first
    
    if  ( (latLongAry.length !=2) || isNaN(latLongAry[0])  || isNaN(latLongAry[1]) ) {
      this.dialogService.alert('Latitude,Longitude format must be: number,number');
      return;
    }

    eventData['lat'] = parseFloat(latLongAry[0]);
    eventData['lng'] = parseFloat(latLongAry[1]);

    delete eventData['latLongitude'];


    eventData['type'] = this.type;


    //--- Incident specific
    if (this.type == 'Incident') {   

      let date: Date;
      if (! this.form.value.currentTime) {
        date = new Date(eventData['dateTime'].replace(/T/, ' ')); 
      } else {
        date = new Date();  
      }

      // fix the time 
      eventData['time'] = date.toString();
      delete eventData['dateTime'];
      delete eventData['currentTime'];

      eventData['hour'] = date.toTimeString().substring(0,5);


      // ensure we use previous time on edit
      this.form.value.currentTime = false;
      this.form.value.dateTime = date.toString();

    } 


    //--- Event specific
    if (this.type == 'Event') {  

      // save with full date format
      if (eventData['startTime1']) {
        // the current format is short (2016-09-27T14:55)
        eventData['startTime'] = eventData['startTime1'].substring(11); // just hour:min
        let startDate = new Date(eventData['startTime1'].replace(/T/, ' ')); 
        eventData['startTime1'] = startDate.toString(); // keep the full format in DB
        this.form.value.startTime1 = startDate.toString(); // and for edit
      }
      if (eventData['endTime1']) {
        // the current format is short (2016-09-27T14:55)
        eventData['endTime'] = eventData['endTime1'].substring(11); // just hour:min
        let endDate = new Date(eventData['endTime1'].replace(/T/, ' ')); 
        eventData['endTime1'] = endDate.toString(); // keep the full format in DB
        this.form.value.endTime1 = endDate.toString(); // and for edit
      }

    } 


    // store data for future edit 
    eventData['eventEditCfg'] = this.qs.getFilledQuestionsCfg(this.type, this.form.value);



    // final steps
    
    if (this.isAddMode) { 

      // insert it to the DB
      this.els.addEvent(eventData);


      // avoid re-submit - clear the form
      this.questions = this.qs.getQuestions(this.type); 
      this.form = this.qcs.toFormGroup(this.questions);     
  
    }
    else { // Edit/Copy

      // insert it to the DB
      if (this.copyMode) {
        this.els.addEvent(eventData); // Like Add
      } else {
      	this.els.updateEvent(this.eventKey, eventData, this.oldEvent);

      }

    	this.readyForEdit = null; // will clear the page by *ngIf  
   
   		this.router.navigate(['/show']);
   	}
   	
  }



  formReady() {
    // either user left currentTime checked or filled the date 
    return (
        this.form.controls['currentTime'].value || 
        (this.form.controls['dateTime'].value != "")
        );

  }



  onCancel() {


    if (!this.isAddMode) { // edit/copy

      this.readyForEdit = null; // will clear the page by *ngIf  
   
      this.router.navigate(['/show']);

    } else {

      this.questions = this.qs.getQuestions(this.type); 
      this.form = this.qcs.toFormGroup(this.questions);  
      this.router.navigate(['/show']);

    }


  }
    

  mapClicked(event) {

    //console.log("mapClicked:", event);

    let coords1 = event.coords.lat.toFixed(6) + ", " + event.coords.lng.toFixed(6);

    this.form.controls['latLongitude'].setValue(coords1);


  }


  
}
