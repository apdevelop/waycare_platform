import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-pre-section',
  templateUrl: './pre-section.component.html',
  styleUrls: ['./pre-section.component.css']
})
export class PreSectionComponent implements OnInit {

  @Input() section;
  @Input() traffic;
  @Input() weather;
  @Input() incidents;

  @Input() sliderTime;
  constructor() { }

  ngOnInit() {
  }


  // for slider
  getStyle() {

    let defStyle = {
      value: 0.1,
      color: '#E8E8E8',
      title: 'Not selected'
    };

    if (this.section.style) {
      if (this.section.style[this.sliderTime]) {
        return(this.section.style[this.sliderTime]);
      }
    }
    return(defStyle);
  }
}
