


export const formConfig_fll = {
	
  Common: {




    titleQ: {
      type: 'Textbox',
      cfg: {
        key: 'title',
        label: 'Title',
        value: '',
        required: true,  
        order: 10
      }
    },
    



    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Latitude,Longitude',
        type: 'text',    
        value: '',
        required: true,  
        order: 30
      }
    },



    classQcheckBox: {
      type: 'MultiCheckbox',
      cfg: {
        key: 'affectedSections',
        label: 'Affected sections',
        useKeyAsValue: true,
        options: [
          {key: '0', text: 'West Sunrise Blvd.', value: false},
          {key: '1', text: 'East Sunrise Blvd.', value: false},
          {key: '2', text: 'West Broward Blvd.', value: false},
          {key: '3', text: 'East Broward Blvd.', value: false},
          {key: '4', text: 'East Las Olas Blvd.', value: false},
          {key: '5', text: 'I-95- int 25 to int 27', value: false},
          {key: '6', text: 'I95- int 27 to int 31', value: false},
          {key: '7', text: 'I95- int 31 to int 36', value: false},
          {key: '8', text: 'North Powerline Rd.- North of Oakland Park Blvd.', value: false},
          {key: '9', text: 'South Powerline Rd.- South of Oakland Park Blvd.', value: false},
          {key: '10', text: 'East Davie Blvd.', value: false},
          {key: '11', text: 'West Davie Blvd.', value: false},
          {key: '12', text: 'East Marina Mile Blvd.', value: false},
          {key: '13', text: 'West Marina Mile Blvd.', value: false},
          {key: '14', text: 'US-1', value: false},
          {key: '15', text: 'US-1- North of Sunrise Blvd.', value: false},
          {key: '16', text: 'SW 9th Av.', value: false},
          {key: '17', text: 'SE 17th St.', value: false},
          {key: '18', text: 'SeaBreeze Blvd.', value: false},
          {key: '19', text: 'North Ocean Blvd.', value: false},
          {key: '20', text: 'NE 3rd Av.', value: false},
          {key: '21', text: 'NE 4th Av.', value: false},
        ],
        order: 90
      }
    },


  },




  Incident: {




    timeCbQ: {
      type: 'Checkbox',
      cfg: {
        key: 'currentTime',
        text: 'Current time',
        label: 'Time', 
        value: true, // checked
        order: 20
      }
    },


    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Date-Time',
        value: '',
        required: false,  
        hideCond: 'currentTime',     // hidden when currentTime is checked  
        order: 21
      }
    },



    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'construction', // checked key
        options: [
          {key: 'construction',  value: 'Construction'},
          {key: 'crash-major',  value: 'Crash-major'},
          {key: 'crash-minor',  value: 'Crash-minor'},
          {key: 'incident',  value: 'Incident'},
        ],
        order: 30
      }
    },

  

    incidentTypeQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'incidentType',
            label: 'Incident type',
            value: 'construction', 
            options: [
              {key: 'construction',  value: 'Construction'},
              {key: 'crash',  value: 'Crash'},
              {key: 'road-incident',  value: 'Road-incident'},
              {key: 'other',  value: 'Other'},
            ],
            order: 50
          }
        },




  },




  Event: {


    startTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'startTime1',
        type: 'datetime-local',
        label: 'Start-Time',
        value: '',
        required: false,  
        order: 20
      }
    },


    endTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'endTime1',
        type: 'datetime-local',
        label: 'End-Time',
        value: '',
        required: false,  
        order: 21
      }
    },



    descQ: {
      type: 'Textbox',
      cfg: {
        key: 'description',
        label: 'Description',
        value: '',
        required: false,  
        order: 40
      }
    },
    


    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'concert', // checked key
        options: [
          {key: 'concert',  value: 'Concert'},
          {key: 'conference',  value: 'Conference'},
          {key: 'crowd',  value: 'Crowd'},
          {key: 'show',  value: 'Show'},
          {key: 'stadium',  value: 'Stadium'},
        ],
        order: 50
      }
    },

  



  },




};


