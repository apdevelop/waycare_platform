


export const formConfig_lv = {
	
  Common: {




    titleQ: {
      type: 'Textbox',
      cfg: {
        key: 'title',
        label: 'Title',
        value: '',
        required: false, 
        disabled: false, 
        order: 10
      }
    },
    



    classQcheckBox: {
      type: 'MultiCheckbox',
      cfg: {
        key: 'affectedSections',
        label: 'Affected sections',
        useKeyAsValue: true,
        options: [
          {key: '0', text: 'West Desert Inn Rd.', value: false},
          {key: '1', text: 'East Desert Inn Rd.- West of Eastern Av.', value: false},
          {key: '2', text: 'East Desert Inn Rd. - East of Eastern Av.', value: false},
          {key: '3', text: 'I15 - int. 35-42a', value: false},
          {key: '4', text: 'I15 - int. 33-35', value: false},
          {key: '5', text: 'West Flamingo Rd.', value: false},
          {key: '6', text: 'East Flamingo Rd.- East of Eastern Av.', value: false},
          {key: '7', text: 'East Flamingo Rd.- West of Eastern Av.', value: false},
          {key: '8', text: 'North Las Vegas Blv.', value: false},
          {key: '9', text: 'South Las-Vegas Blvd.- North of Sands Av.', value: false},
          {key: '10', text: 'South Las-Vegas Blvd.- South of Sands Av.', value: false},
          {key: '11', text: 'Paradise Rd.- North of Tropicana Av.', value: false},
          {key: '12', text: 'Paradise Rd.- South of Tropicana Av.', value: false},
          {key: '13', text: 'West Tropicana Av.', value: false},
          {key: '14', text: 'East Tropicana Av.- West of Eastern Av.', value: false},
          {key: '15', text: 'East Tropicana Av.- East of Eastern Av.', value: false},
          {key: '16', text: 'West Sahara Av.', value: false},
          {key: '17', text: 'East Sahara Av.- West of Eastern Av.', value: false},
          {key: '18', text: 'East Sahara Av.- East of Eastern Av.', value: false},
          {key: '19', text: 'Spring Montain Rd.', value: false},
          {key: '20', text: 'West Harmon Av.', value: false},
          {key: '21', text: 'East Harmon Av.- West of Eastern Av.', value: false},
          {key: '22', text: 'East Harmon Av.- East of Eastern Av.', value: false},
          {key: '23', text: 'North Maryland Parkway', value: false},
          {key: '24', text: 'South Maryland Parkway', value: false},
        ],
        order: 90,
        disabled: true,
      }
    },


  },




  Crash: {




    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Crash Time',
        value: (new Date()).toLocaleString(),
        icon: 'fa fa-clock-o',
        required: true,  
        order: 10
      }
    },



    crashTypeQ: {
      type: 'Dropdown',
      cfg: {
        key: 'crashType',
        label: 'Crash Type',
        value: 'crash-minor', 
        options: [
          {key: 'crash-minor',  value: 'Minor Crash'},
          {key: 'crash-major',  value: 'Major Crash'},
        ],
        icon: 'fa fa-id-card-o',
        order: 20,
        required: true,
      }
    },




    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Location',
        type: 'text',    
        value: '',
        required: true,  
        icon: 'fa fa-map-marker',
        order: 30
      }
    },



    LaneQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'lane',
            label: 'Lane',
            value: 'left', 
            options: [
              {key: 'left',  value: 'Left Lane'},
              {key: 'middle',  value: 'Middle Lane'},
              {key: 'right',  value: 'Right Lane'},
            ],
            icon: 'fa fa-road',
            order: 40          
          }
        },


    DamageQ: {
      type: 'Radio',
      cfg: {
        key: 'damage',
        label: 'Damage',
        useKeyAsValue: true,
        value: 'property_damage', // checked key
        options: [
          {key: 'property_damage',  value: 'Property Damage'},
          {key: 'injeries',  value: 'Injuries'},
          {key: 'fatalities',  value: 'Fatalities'},
        ],
        icon: 'fa fa-user-md',
        order: 50
      }
    },

  


  },





  Construction: {



    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Construction Time',
        value: (new Date()).toLocaleString(),
        icon: 'fa fa-clock-o',
        required: true,  
        order: 10
      }
    },



    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Location',
        type: 'text',    
        value: '',
        required: true,  
        icon: 'fa fa-map-marker',
        order: 30
      }
    },



    LaneQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'lane',
            label: 'Lane',
            value: 'left', 
            options: [
              {key: 'left',  value: 'Left Lane'},
              {key: 'middle',  value: 'Middle Lane'},
              {key: 'right',  value: 'Right Lane'},
            ],
            icon: 'fa fa-road',
            order: 40          
          }
        },




  },





  Incident: {



    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Incident Time',
        value: (new Date()).toLocaleString(),
        icon: 'fa fa-clock-o',
        required: true,  
        order: 10
      }
    },



    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Location',
        type: 'text',    
        value: '',
        required: true,  
        icon: 'fa fa-map-marker',
        order: 30
      }
    },



    LaneQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'lane',
            label: 'Lane',
            value: 'left', 
            options: [
              {key: 'left',  value: 'Left Lane'},
              {key: 'middle',  value: 'Middle Lane'},
              {key: 'right',  value: 'Right Lane'},
            ],
            icon: 'fa fa-road',
            order: 40          
          }
        },




  },









  Event: {


    startTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'startTime1',
        type: 'datetime-local',
        label: 'Start-Time',
        value: (new Date()).toLocaleString(),
        required: false,  
        icon: 'fa fa-clock-o',
        order: 20
      }
    },


    endTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'endTime1',
        type: 'datetime-local',
        label: 'End-Time',
        value: (new Date()).toLocaleString(),
        required: false,  
        icon: 'fa fa-clock-o',
        order: 21
      }
    },



    eventTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'eventType',
        label: 'Event Type',
        useKeyAsValue: true,
        value: 'concert', // checked key
        options: [
          {key: 'concert',  value: 'Concert'},
          {key: 'conference',  value: 'Conference'},
          {key: 'crowd',  value: 'Crowd'},
          {key: 'show',  value: 'Show'},
          {key: 'sport',  value: 'Sport'},
        ],
        icon: 'fa fa-id-card-o',
        order: 30
      }
    },

  


    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Location',
        type: 'text',    
        value: '',
        required: true,  
        icon: 'fa fa-map-marker',
        order: 40
      }
    },



    descQ: {
      type: 'Textbox',
      cfg: {
        key: 'description',
        label: 'Description',
        value: '',
        required: false,  
        order: 50
      }
    },
    




  },




};


