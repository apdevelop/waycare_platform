


export const formConfig_tampa = {
	
  Common: {




    titleQ: {
      type: 'Textbox',
      cfg: {
        key: 'title',
        label: 'Title',
        value: '',
        required: true,  
        order: 10
      }
    },
    



    LatLongitudeQ: {
      type: 'Textbox',
      cfg: {
        key: 'latLongitude',
        label: 'Latitude,Longitude',
        type: 'text',    
        value: '',
        required: true,  
        order: 30
      }
    },


    classQcheckBox: {
      type: 'MultiCheckbox',
      cfg: {
        key: 'affectedSections',
        label: 'Affected sections',
        useKeyAsValue: true,
        options: [
                {key: '0', text: 'W Columbus Dr.- West of I-275.txt', value: false},
                {key: '1', text: 'E Columbus Dr.- East of I-275.txt', value: false},
                {key: '2', text: 'I-275 North of I-41', value: false},
                {key: '3', text: 'I-275 East of I-92', value: false},
                {key: '4', text: 'I-275 East of US-589', value: false},
                {key: '5', text: 'East Lake Av.', value: false},
                {key: '6', text: 'N 15St- North of I-41', value: false},
                {key: '7', text: 'N 15St- North of I-4', value: false},
                {key: '8', text: 'N 22nd S- Nnorth to Selmon Expressway', value: false},
                {key: '9', text: 'N 22nd St- North to I-41', value: false},
                {key: '10', text: 'Hanna Av.', value: false},
                {key: '11', text: 'E Dr VLK jun Blvd- East of I275', value: false},
                {key: '12', text: 'E Dr VLK jun Blvd- East of I275', value: false},
                {key: '13', text: 'E Dr VLK jun Blvd- West of I275', value: false},
                {key: '14', text: 'Osborne Av.', value: false},
                {key: '15', text: 'Nebraska Av.- South of US-41', value: false},
                {key: '16', text: 'Kennedy Blvd- West of I-92', value: false},
                {key: '17', text: 'Kennedy Blvd- East of I-92', value: false},
                {key: '18', text: 'E 21st Av.', value: false},
                {key: '19', text: 'Selmon Expressway- East of I-92', value: false},
                {key: '20', text: 'Selmon Expressway- East of I-45', value: false},
                {key: '21', text: '34th St.', value: false},
                {key: '22', text: '34th St- north of I-4', value: false},
                {key: '23', text: '34th St- north of I-41', value: false},
                {key: '24', text: 'I-583 North of Selmon Expressway.txt', value: false},
                {key: '25', text: 'I-583 North of I-92', value: false},
                {key: '26', text: 'I-4 East- int 0 to int 3', value: false},
                {key: '27', text: 'I-4 East- East of int 3', value: false},
                {key: '28', text: 'I-4 West- int 0 to int 3', value: false},
                {key: '29', text: 'I-4 West- East of Int 3', value: false},
                {key: '30', text: 'North 31st St.', value: false},
                {key: '31', text: 'North 40th St.', value: false},
                {key: '32', text: 'South 40th St.', value: false},
                {key: '33', text: 'East US-41', value: false},
                {key: '34', text: 'West US-92', value: false},
                {key: '35', text: 'US-92', value: false},
                {key: '36', text: 'North Nebraska Av.', value: false},

        ],
        order: 90
      }
    },


  },




  Incident: {




    timeCbQ: {
      type: 'Checkbox',
      cfg: {
        key: 'currentTime',
        text: 'Current time',
        label: 'Time', 
        value: true, // checked
        order: 20
      }
    },


    dateTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'dateTime',
        type: 'datetime-local',
        label: 'Date-Time',
        value: '',
        required: false,  
        hideCond: 'currentTime',     // hidden when currentTime is checked  
        order: 21
      }
    },



    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'construction', // checked key
        options: [
          {key: 'construction',  value: 'Construction'},
          {key: 'crash-major',  value: 'Crash-major'},
          {key: 'crash-minor',  value: 'Crash-minor'},
          {key: 'incident',  value: 'Incident'},
        ],
        order: 30
      }
    },

  

    incidentTypeQ: { 
       type: 'Dropdown',
       cfg: { 
            key: 'incidentType',
            label: 'Incident type',
            value: 'construction', 
            options: [
              {key: 'construction',  value: 'Construction'},
              {key: 'crash',  value: 'Crash'},
              {key: 'road-incident',  value: 'Road-incident'},
              {key: 'other',  value: 'Other'},
            ],
            order: 50
          }
        },




  },




  Event: {


    startTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'startTime1',
        type: 'datetime-local',
        label: 'Start-Time',
        value: '',
        required: false,  
        order: 20
      }
    },


    endTimeQ: {
      type: 'Textbox',
      cfg: {
        key: 'endTime1',
        type: 'datetime-local',
        label: 'End-Time',
        value: '',
        required: false,  
        order: 21
      }
    },



    descQ: {
      type: 'Textbox',
      cfg: {
        key: 'description',
        label: 'Description',
        value: '',
        required: false,  
        order: 40
      }
    },
    


    iconTypeQ: {
      type: 'Radio',
      cfg: {
        key: 'icon',
        label: 'Icon type',
        useKeyAsValue: true,
        value: 'concert', // checked key
        options: [
          {key: 'concert',  value: 'Concert'},
          {key: 'conference',  value: 'Conference'},
          {key: 'crowd',  value: 'Crowd'},
          {key: 'show',  value: 'Show'},
          {key: 'stadium',  value: 'Stadium'},
        ],
        order: 50
      }
    },

  



  },




};


